<!DOCTYPE html>
<html lang="en">
<head>

    <script src='https://www.codejika.com/js/merged.js' type='text/javascript' id="js-jquery"></script>

    <script type="text/javascript">
        var auth_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";
        var lesson_id = "<?php echo htmlspecialchars($_GET["lesson_id"]); ?>";
        console.log(auth_id);
        console.log(lesson_id);
        //var auth_id = "1adcmgcs0el";
        //var lesson_id = "P000-5MIN";

        function initFirebase() {
            // Initialize Firebase
            var config = {
                apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
                authDomain: "codejika-2cf17.firebaseapp.com",
                databaseURL: "https://codejika-2cf17.firebaseio.com",
                projectId: "codejika-2cf17",
                storageBucket: "codejika-2cf17.appspot.com",
                messagingSenderId: "405485160215"
            }

            firebase.initializeApp(config);
        }

        function getLessonProgressPromise(default_code) {

            return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id).once('value').then(function (snapshot) {
                return snapshot.val();
            }, function (error) {
                console.log(error);
            });
            console.log("getLessonProgressPromise");
        }


        initFirebase();

        var getLessonProgress = getLessonProgressPromise();

        //Promise.all([getLessonData, getUserProfile, getUserSkills, getLessonProgress]).then(function (results) {  P0 disabled
        Promise.all([getLessonProgress]).then(function (results) {

            lesson_progress = results[0];

            document.open();

            if (lesson_progress.user_code) {
                document.write(lesson_progress.user_code);
            } else {
                document.write("<html><head></head><body><h2 class='no_preview' style='height:100%;width:100%;color:#bbb;font-size:16px;font-family:'Rajdhani',sans-serif;text-transform:uppercase;font-weight:bold;position:absolute;top:35%;text-align:center;margin:auto;padding:0;'>No preview available<br><br>Time to get coding</h2></body></html>");
            }

            //document.querySelector('head').innerHTML += '<link rel="stylesheet" href="https://www.codejika.com/css/reset.css" type="text/css"/>';
            document.querySelector('head').innerHTML += '<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">';
            document.querySelector('head').innerHTML += '<meta property="og:title" content="Check out my website I just made on CodeJIKA.com"</title>';
            document.querySelector('head').innerHTML += '<meta name="og:description" content="5 minute website: Learn how to build your first website in just 5 minutes">';
            document.querySelector('head').innerHTML += '<meta property="og:url" content="https://www.codejika.com/' + lesson_id + '/' + auth_id + '">';
        });
    </script>
</head>
<body>

</body>
</html>