<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        var start = Date.now();
    </script>
    <meta charset="utf-8">
    <title>Code JIKA - Learn HTML, CSS and Javascript</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta id="description" name="description" content="CodeJIKA Learn">
    <meta id="keywords" name="keywords" content="coding, learn to code, codejika, africa, south africa">
    <link href="<?php echo $home_url; ?>/css/merged.min.css" rel="stylesheet" type="text/css"/>


    <script type="text/javascript">

        function randText() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }

        document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/mobile-lessons-m.css?" + randText() + "'\/>");
    </script>
    <style>

        body, #header {
            max-height: 800px;
        }


    </style>
</head>

<body>

<div id="header" class="embed-nav">
    <a href="https://<?php echo $_SERVER['SERVER_NAME'] ?>" style="padding: 0!important;"><img
                src="<?php echo $home_url; ?>/img/logo.png" alt="logo" id="logo" style=
        ""/></a>

    <ul id="lesson-navbar" class="hXide2" style="position: absolute; right: 0; top: 0; border: 0;">
        <li id="menu-lesson" class="active"><a><span>LESSON</span><i class="icon-assignment"></i></a></li>

        <li id="menu-code"><a><span>CODE</span><i class="icon-code1"></i></a></li>

        <li id="menu-preview"><a><span>PREVIEW</span><i class="icon-personal_video"></i></a></li>

        <li id="menu-skills" class="hide"><a><span>SKILLS</span><i class="icon-school"></i></a></li>
        <li id="menu-profile"><a><span>MENU</span><i class="icon-dehaze"></i></a></li>
    </ul>
</div>
<div class="swiper-container swiper-container-h">
    <div class="swiper-wrapper">
        <div class="swiper-slide" id="lesson-page">
            <div class="swiper-container swiper-container-v">
                <div class="swiper-wrapper cm-s-base16-dark">

                    <div class="loading">
                        <div class="spinner">
                            <div class="rect1"></div>
                            <div class="rect2"></div>
                            <div class="rect3"></div>
                            <div class="rect4"></div>
                            <div class="rect5"></div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination swiper-pagination-v"></div>
            </div>
        </div>
        <div class="swiper-slide" id="editor_page">

        <textarea id='code-editor' name='editor' style="display: none;">









		</textarea>
            <div id="virt-keyboard" class="hide">

            </div>
        </div>
        <div class="swiper-slide" id="preview_page">
            <iframe class="" id='preview' name="preview"></iframe>

            <div class="swipe-overlay"></div>
            <div id="disallow-interact" class="icon-locked">
                <i class=" icon-uniE011 fs2"></i>
                <div>Swipe Mode</div>
            </div>
            <div id="allow-interact" class="icon-unlocked hide">
                <i class="icon-uniE010 fs2"></i>
                <div>Input Mode</div>
            </div>
        </div>
        <div class="swiper-slide" id="profile_page">
            <div class="container">
                <i class="icon-account_circle fs5" style="padding: 30px 0 10px 0; display: block;"></i>
                <h2 class="profile_name">Hello</h2>
                <div class="menu">
                    <ul>
                        <li class=""><a class="divLink" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>"></a><i
                                    class="icon-home fs2"></i> Homepage
                        </li>
                        <li class="reset-lesson" data-toggle="modal" data-target="#reset-lesson"><i
                                    class="icon-replay fs2"></i> Reset Current Lesson
                        </li>
                        <li class=""><i class="icon-face fs2"></i> Instructor
                            <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                            <div class="option_unselected select_lego">Lego</div>
                        </li>
                        <li id="share-link" class="hide"><i class="icon-assignment fs2"></i> Share on:
                            <div class="option_unselected facXebook-share" data-js="facebook-share"
                                 style="margin-left: 5px;">FB
                            </div>
                            <div class="option_unselected twitXter-share" style="margin-left: 5px;"
                                 data-js="twitter-share">TW
                            </div>
                            <a id="whatsapp" href="">
                                <div class="option_unselected ">WA</div>
                            </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="swiper-pagination swiper-pagination-h"></div>
</div>

</div>


<div class="modalPlaceholder"></div>

<div class="success-animation hide" style="">
    
 <span class="success-layers">
    <svg class="success-svg-icon" style="color: #fff;" aria-hidden="true" data-prefix="fas" data-icon="circle"
         role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                                                    d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path></svg>
    <svg class="success-svg-icon" style="color: #00A921;" aria-hidden="true" data-prefix="fas" data-icon="check-circle"
         role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor"
                                                                                                    d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path></svg>
  </span>

    </span>
</div>


<script src='<?php echo $home_url; ?>/js/merged.js' type='text/javascript' id="js-jquery"></script>


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-63106610-3', 'auto');
    ga('send', 'pageview');

</script>


<script>

    var lessons_data = {
        "5-minute-website-dev": {lesson_id: "P001-L00-M-V003", save_id: "5-minute-website-dev"},
        "5-minute-website": {lesson_id: "P001-L00-M-V002", save_id: "5-minute-website"},
        "10-minute-website": {lesson_id: "P001-L00-M-V001", save_id: "10-minute-website"},
        "javascript-intro-the-party": {lesson_id: "P000-J01-M-V001", save_id: "javascript-intro-the-party"},
        "my-first-landing-page-1": {lesson_id: "P001-T01-M-V001", save_id: "my-first-landing-page-1"}
    };


    var lessson_url = "<?php echo htmlspecialchars($_GET["lesson_id"]); ?>";
    var set_user_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";

    var lesson_id = lessons_data[lessson_url]["lesson_id"];
    var save_lesson_id = lessons_data[lessson_url]["save_id"];

    console.log("lesson_url: " + lessson_url);
    console.log("lesson_id: " + lesson_id);
    console.log("set_user_id: " + set_user_id);
    console.log("save_lesson_id: " + save_lesson_id);

    var timeout;
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);

        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (swiperTabs.activeIndex == 0) {
                swiperLesson.slideTo(swiperLesson.activeIndex - 1);
                console.log("on lesson tab" + swiperLesson.activeIndex);
            } else {
                swiperTabs.slideTo(swiperTabs.activeIndex - 1);
                console.log("on other tab" + swiperTabs.activeIndex);
            }

        }, 50);


    };
    document.write("<script type='text/javascript' src='<?php echo $home_url; ?>/js/mobile-lesson-P00v3.js?" + randText() + "'><\/script>");

</script>

</body>

</html>