<div id="divSection">
    <form id="myForm">
        <label>Name</label><input type="text" name="name"/>
        <label>Age</label><input type="text" name="age"/>
        <input type="button" value="Save"/>
    </form>
</div>
<script src="https://cdn.jsdelivr.net/gh/GrabzIt/grabzit@js-3.2.9.8/javascript/grabzit.min.js"></script>
<script>
    GrabzIt("Sign in to view your Application Key").ConvertPage({
        "target": "#divSection",
        "bheight": -1,
        "height": -1,
        "width": -1
    }).Create();
</script>