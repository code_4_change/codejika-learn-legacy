# Running with docker

You need [docker](https://docs.docker.com/engine/installation/) and
[docker-compose](https://docs.docker.com/compose/install/).

Running:

    docker-compose up

will launch the Docker container in foreground and show the logs. For running in
background add the `-d` flag.

The code is mounted from your host machine into the container, so changes you
make will be automatically reflected.


## Running commands

To execute commands inside the running container you can use:
    
    # Web frontend
    docker-compose exec fe <command>
    
For more information continue reading the main [README](../../README.md).
