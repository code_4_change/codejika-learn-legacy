if (set_user_id === undefined || set_user_id === '') {
  var auth_id = localStorage.getItem('userid')
  if (auth_id == null) {
    auth_id = Math.random()
      .toString(20)
      .replace('0.', '')
    localStorage.setItem('userid', auth_id)
  }
} else {
  var auth_id = set_user_id
  localStorage.setItem('userid', auth_id)
}

if (history.pushState) {
  var newurl =
    window.location.protocol +
    '//' +
    window.location.host +
    window.location.pathname +
    '?' +
    auth_id
  console.log('newurl: ' + newurl)
  window.history.pushState({ path: newurl }, '', newurl)
}

var greetcode = Boolean(localStorage.getItem('greetcode')) || false;
var greetview = Boolean(localStorage.getItem('greetview')) || false;

function refreshValues() {

  greetcode = localStorage.getItem('greetcode') || false;
  greetview = localStorage.getItem('greetview') || false;
}

function showCertificate(first_name, last_name, certificate_key, certificate_time){
  console.log("Inside ", 'showCertificate', first_name, last_name, certificate_time);
  return `
  <div id="saveAsPdf">
                    <div class="certificate-logo">
                        <a ><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z" alt="logo"/>
                        </a>
                      </div>
                      <div class="container text-center">
                        <h3>Certificate</h3>

                        <h6 class="mt-3">${first_name} ${last_name}</h6>
                        <div>
                          <p style="font-size:16px">For the successful completion of <br> ${certificate_key} on <small>${certificate_time}</small></p>
                        </div>
                      </div><br><br>
                      <div style=" display: flex; justify-content: space-around;"> <div class="certificate-logo text-left">
                      <a >
                      <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z" alt="logo" id="logo" style="">
                      </a>
                      <p style="margin-top: 0.5em;">Academic Director</p> </div> <div class="text-right"> <p><strong>Hours:</strong> ${certificate_key}: 2hours</p> <p><strong>Difficulty:</strong> ${certificate_key}: Explorer.</p> <small style="font-size:10px"><a>Certificate Link</a></small> </div></div>
                    </div>
  `
}


var delay
var lesson_data = null
var lesson_progress = null
var slide_data = null
var user_skills = null
var active_slide
var active_tab
var save_pending = false
var checkpoint_id = 0
var checkpoint_count = 0
var checkpoint_completed = 0
var progressPercentage
var current_avatar = 'robot'
var swiperTabs
var swiperLesson
var first_name
var last_name
var nick_name
var DEBUG = true
var dialogInMenu = false;
var subModal= false;
var screenHeight = screen.height + "px";
var screenHeight_plus23 = screen.height + "px";
var userInfo;
var current_FullDate =new Date()


if (!DEBUG) {
  if (!window.console) window.console = {}
  var methods = ['log', 'debug', 'warn', 'info']
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () { }
  }
}

function timeNow() {
  return new Date().toISOString()
}

// Database continuity
var lesson_mappings = {
    "coding1-t1": "P1Training1",
    "coding1-t2": "P1Training2",
    "lesson-m": "P1Training1",
    "P1Training1": "P1Training1",
    "P1Training2": "P1Training1",
}

function getCurrentSlideNumber() {
    var current_lesson = window.location.href;
    console.log(current_lesson)
    for (var key in lesson_mappings) {
        console.log(key)
        console.log(current_lesson);
        console.log(current_lesson.includes(key))
        if (current_lesson.includes(key)) {
            var lesson_id = lesson_mappings[key];
            console.log(lesson_id, 'lesson_id');
            firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/').once('value').then(function (snapshot) {
                data = snapshot.val()
                if(data){
                  var keys = Object.keys(data)
                  var k = keys[Object.keys(data).length - 1]
                  if(data[k].user_code){
                    editor.value = data[k].user_code
                  }
                }

                // return snapshot.val();
            })
        }
    }
}


// Database continuity


// *********************** Open Gallery Page Dialog ****************

var loading = null
$('.create-GalleryPage').click(function () {
  dialogInMenu = true;
  userCodeHtml2 = ''

  createGalleryPage();
  // hideButton()
      document.getElementById('previous').style.visibility = 'hidden'
    document.getElementById('previous2').style.visibility = 'hidden'
  loading = document.getElementById('loadingIcon')
  document.getElementById('pageNumber').innerHTML = current_page + 1
  document.getElementById('pageNumber2').innerHTML = current_page + 1

  getGalleryData();
})

function createGalleryPage() {
  let GalleryInit = document.getElementById('#galleryContainer')

  $.galleryDialog({
    modalName: 'createGalleryPage',
    htmlContent:
      `
      <div class="container" style="height:${screenHeight_plus23}; padding: 23px;overflow-y: scroll;" id="gallery_page">
      <h2 class="">Projects</h2>
      <div class="search-continer">
        <div class="search-list">
          <div class="">
            <h6>Search By:</h6>
          </div>
          <ul>
            <li>
              <select name="" id="selectOptions" onchange="FilterData(this.event)">
                <option value="Projects" id="projects">All Projects</option>
                <option value="Recent" id="recent"> Recent</option>
                <option value="MostLiked" id="likes">Most Liked</option>
              </select>
            </li>
          </ul>
        </div>
      </div>
      <div class="pagination-section">
        <ul class="pagination">
          <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
      <div class="loading" id="loadingIcon">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
      </div>
      <div class="gallery-container" id="galleryContainer">

      </div>
      <div class="gallery-container" id="galleryLikes">

      </div>
      <div class="gallery-container" id="recentProjects">
      </div>
      <div class="pagination-section" id="bottom-pagination">
        <ul class="pagination">
          <li class="page-item previous" id="previous2"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber2"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
    </div>
    </div>`
  })
  console.log('Test');
}
// *************** end of Gallery Dialog **************

// ************ profile Page
$('.create-profilePage').click(function () {
  dialogInMenu = true
  checkLoggedInAndGetUserDetails().then(function(user_details){ createProfilePage(user_details)}).catch(function(err){
    console.log(err, "Here is the error");
     UnRegisteredModal()})
})


function createProfilePage(user_details) {

  console.log("Inside createProfilepage", user_details);
  let ProfileFName;
  let ProfileLName;
  $.galleryDialog({
    modalName: 'createProfilePage',
    htmlContent: `
    <div  id="user_profile" style="height:${screenHeight}; padding: 23px;overflow-y: scroll;">
      <div class="container">
          <i class="icon-account_circle fs6" style="padding: 30px 0px 10px 0; display: block;"></i>
          <div>
            <input class="form-control" id="pf_name" placeholder="First name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.first_name ? user_details.first_name : ''}">
            <input class="form-control" id="pl_name" placeholder="Last name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.last_name ? user_details.last_name : ''}">
            <div class="text-left ml-4"><label style="margin: 0;">Aspiring:</label></div>
            <div class="mb-3"><textarea class="form-control m-auto" id="aspiringContent" placeholder="Dream Job" type="text" cols="4" style="width:90%" >${user_details.details ? user_details.details.aspiring ? user_details.details.aspiring : '' : ''}</textarea></div>
            <input class="form-control" id="Pgrade" placeholder="Grade" name="Grade" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.grade ? user_details.details.grade : '' : ''}">
            <input class="form-control" id="PConuntry" placeholder="Country" name="country" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.country ? user_details.details.country : '' : ''}">
            <button id="action_button" class="btn btn-primary light action ">Save</button>
          </div>
          <br><br>
          <div class="container">
            <h2 class="profile_name mt-4">Certificates:</h2>`
            +
            `<hr>`
            +`
            <div class="certificates-blog" id="certificateBlog">
              ${user_details.certificate ? prepareCertificates(user_details) :'<p>Please complete the Course to get the certificates</p>'}
            </div>`+
            `<hr>`+
          `
          </div>
      </div>
    </div>
    `
  })
  // let hideSaveBtn = document.getElementById('pf_name').value
  $('.action').click(function (){
    let ProfileFName = document.getElementById('pf_name').value;
    let ProfileLName = document.getElementById('pl_name').value;
    let Aspiring = document.getElementById('aspiringContent').value;
    let grade = document.getElementById('Pgrade').value;
    let p_country = document.getElementById('PConuntry').value

    // store to users
    var ref = firebase.database().ref('/users/' + auth_id)

    ref.update({
      first_name : ProfileFName,
      last_name: ProfileLName,
      'details/country' : p_country,
      'details/aspiring': Aspiring,
      'details/grade': grade
    })

    document.getElementById('action_button').style.display = 'none';
    successProfileDetails('Profile Updated', 'Thank you for updating your profile')
  })
  console.log("Completed task");

}
function prepareCertificates(user_details) {
  var sectionCertificate = ''
  var certificates = Object.keys(user_details.certificate)
  certificates.map(element => {
    sectionCertificate +=`<h6 class="mt-3">${element}</h6>
        <div class="certificates-container">
          <a class="certificate-card Card_pdf_${element}" onclick="exportPdfCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}',showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate PDF </div>
            <strong style="display-block">PDF</strong>
          </a>
          <a class="certificate-card Card_html_${element}" onclick="openHtmlCertificate(showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate html </div>
            <strong style="display:block">HTML</strong>
          </a>
        </div>`
  });
  return sectionCertificate
}

function openHtmlCertificate(pageContent) {
  console.log("Inside ", 'openHtmlCertificate');
  subModal = true
  $.nestedModal1({
    modalName: 'HtmlCertificate',
    htmlContent:pageContent,
    closeExisting:false
  })
}

function exportPdfCertificate(first_name, last_name, certificate_key, certificate_time){
  window.event.preventDefault()
 var docDefinition = {
    	content: [
	
      {
        image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z',
        style: 'logo'
      },
	 	  {
		    text: 'Certificate',
		    style: 'header'
	    },
      {
        text: 'Congratularions '+ `${first_name} ` + `${last_name}`+ ' for succefull completion of ', 
        style: 'content'
      },
      {
        text: `${certificate_key}` + ' on ' + `${certificate_time} \n\n`,
        style: 'content'
      },
		{image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAqALADASIAAhEBAxEB/8QAHgAAAQQDAQEBAAAAAAAAAAAACQAFBwgEBgoDAgH/xABTEAABAwMDAgIEBggPEQAAAAABAgMEBQYRAAcIEiEJExQxQVEVFiJhcbMXGSMyN1didhgkMzY4OUJ1gZGVlrTS0zVHUlNWWGVmcnN0d4OjsbLU/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ABb0ih1q4JYgUGjzalJI6gzEjrecx7+lIJ051PbvcCiQ11Gs2NcMCK0MrflUx9ptP0qUkAaNNx4l2Hwt8OWmb50ayWanUHrdh3HVfKUlmRU5UxxsNoceKVEIR5yEjsQlKCQCSc4vDbxLE8t91JO0Fd2cZt7z6TInNSW6t6a24GykLacbUyjsUrPfJ9WCO+QAQ9PlGsW97ii+nW/Z1cqcbJT50OnvPIyPWOpCSM6u3vRxg21j+KVRtlqfR2odm3NV6dVH6WwPLaQ06z58iOgDHQ2tTbgATjpSvAxgavHzT520zg3ULP28tTZ+HWm6nTXJLTLU8U6LCjtr8tDbaENLz3CuwCQAB689gBvW7YuS2nUMXHb1TpTjoyhE2I4wpQ94CwM6VDtm5LnfcjW3b9SqzzKPMcbgxHH1ITnGSEAkDPt0dzbrcKxvEh4f3LVb124YpLby6hSjFefTLMKayylbUqO8UJKVJ8xtQPSDkKScj11B8ED8I+6A/wBCQPr16AfP2Jt1B/ezuv8AkaT/AFNa3MhTKdKchVCI9GkMq6XGXmyhaD7ik9wdGC348XWo7J733XtOrYmNV4lr1VVPVOFwqZcfQnGVhv0dQSe/q6j9OvvxcdubDvnjTbfISFQ2YlxRZ1PDc0NpS+9AltKPkOkff9Ki2pOSekhWPvjkBI0ewb6uGEKlQLKr1SiKUUCRDprzzZUPWOpCSMjWd9ibdT8Wd1/yNJ/qaM74cV1rsXw5Y17JhCaq3o9x1VMYudAeLD8h3o6sHp6unGcHGfUdV7+3kV//ADcKf/Ohf/y6AXzzL0d5ceQ0tp1pRQtC0lKkqBwQQe4IPs18afb9ut2+76uO+H4aIjlw1aZVVx0LK0sqfeU6UBR7kArxn240xaBaK8x4WW22/wDxl213J2wqvxLvmp2fS5cwO9b1NqchUVsrW6jutlajklbeRkklsk50KHXSFwyV1cS9oCf8jKSP4oyNBzw7jbf3JtZfVe27u1hlqsW5Pdp05LDodbDyCQelY7EHGQfdr425hQ6luFa9OqEZuRFlVmEw+y4MpcbU+gKSR7iCQdFq4w7f2VuVza5c2buBa1Or9FnTmA/Cnx0utq/TDuFAH71QzkKGCD3BB1BHNTglt9xY3K2z3B2wr1QFDua8IkH4Dm/djBcS4hzLb+epTeAR0rBUO3ylewJM5aeD/RnY1Sv/AIy1humKZQ5KkWtVHyWCkZUoRZCsqR2Bwh3I/LSO2hP66kru/WpWv3uk/Vq1zE2NZdx7jXjRrDtGnrnVmvzWqfCjp/duuKCRk+xIzkk9gASew0DHpamfkTxD304wVcwtzbScTS3XC3DrsDL9Nle4JeAHQr8hwJX82O+pA4FcJZnMG8K0it1efQrPtyKFTqnEbSp1yW5+ox2+sFOcBa1HBwlIHYrSdBVjS0XS6fBK28btuqO2bu/cztcREeVTmp8eP6O5JCCW0OFKQoIKsAkdwDnvjGhMVyiVa2q1Ptyv096DU6XJdhzIryelxh9tRQtCh7CFAg/RoDObvftN0H8wLb/pEPQbrTqV30mstzLGqFYh1YIUlt2kuutyAkj5QBaIVjHr+bRstj6FbHMnw06Ts5a14xqfONtw7dnu9AeXT50JxtQS80CCErLCFDuMocBGmDhF4aNz8Vt43N2br3QpNbQzSZNPjw4MFxvKnSjK1rcPYBKT2A7kjuMdwHlwfqN21XnVtfMvefV5lXVWAHnaq665JIEZzpClOkqxjGM+zGrCeN1+GHbr82n/AOlK0x72cittB4rlF3Vj12M/als1em0iZVWlhTA8tjyH3godlIbW4vKhkENkjIxq5PO/gVVuaFds++rL3NpNH+CaY5DIkxlSGJTLiw626242r51ewgggg+8NZ8I39hRcv5zVf+hxtQL4IH4R90P3kgfXr1cDZna2g+Hhw6umn7jX9BqTcNdRrMiWhox23JDzKG2orKVEqWtXlISn2lSj2xqoHggA/ZH3QOO3wJA+vXoKk88/2Yu7X5yP/wDhOiYeJL+1z0D/AGrb+qGtG5B+Ejf+9e+t37qQ94LfpdPuirLnoju05915htWOxwoJURj3gH5tbJ4t95WfYHFS2NjhW2pNenzqciJFKx55hw2lBclaR96kqDaR7yo4z0nAbv4bPxW+13Qvj15fxb8q4vhjzevo9B9Ikef1dHyseX156e/u76gXck+DV9jq6PiUKH8YfgWb8EeSK95npvkL8jp6/k58zpx1dvf21Ofh0WrJvjw4GbKiy24j9wxbkpbT7iSpDSn35DQWQO5AKskD3arH9pD3U/Hhan8nydANbS093xa0qxb1uCyZ0lqRJt6qS6U880CEOLYdU2pSc98EoJGe+DrAo9IqVwVaFQqNDXLn1GQ3FisIx1OurUEpSM9skkDQYeuj/hWeriRtCf8AU6mD/sJ1RDip4Orjnod58p6mW0/JdbtKlyPlH8mXJQe3zoZP/UHcaJi4/t3svYLSZMqj2jaFswkMNqedRGiQozaQlKcqIAAAAA9Z+c6ClfC8Y8QPlb/xsb65zX74s39zNi/+Ykb/ANRqMOG3JvYiNzs35uKduNToFJ3DmMptudOC40ecpDqsgLcADZPUOkL6er2d+2pO8WRSV0rYpaFBSVbhxiCDkEdI0F5bu/WpWv3uk/Vq0MfwdOK3kszuU95U35boepVpIdR6k90SpifpOWUn3B73g6Jvd4zadaBz3p0n1f7pWqs+GHyHoO9nGyj2q2zDg3Dt1HYoFShR0BtJZQjEaSlI9QcQkhR/xiHPm0Db4m2+cy0drIPHyw4DdXv/AHgeTQ6fTw0l1bcRa0odd6VZAUpSktIJxgqUsHLepv4n8eqHxj2PoG1tLDL05hv0ytTW049NqLgBed9/SCAhGe4QhA9mqkc96BWeOXKLbPnhBpz1ft6C8zQLjhPjzvQEFLjaXWAr9T62nXekjADyAScunUleIbzIouz3GyLL23uJmTcO6MLyLblRXMluA62lT09J9Yw24lKD6wt1B/cnQT1sryT2u38rF70TbyselybDrSqNUArpw6QPkyGsE9TKlpdQlXtLSjjGCRr+MLxW+LF0w+Tlm03pplxOIp9zNtJ+SxPCcMySB6g6hPQo+rrQCclzVTOFHJaocXN+aNfq3Xl29OPwZccVGT51PcUOpYT7VtqCXU+0lHTnCjoiPO7xI+OMjbq5NkLKpsfdCVcUBcKW+y8UUqH1AFLgfHd11CulaQ12CkjKwRjQCOta97zsaYuo2Td1at+W4kIW/Sp7sRxSfcVNqSSNP9X333wuCnvUivby3zUoMhJQ9FmXFMeacSfWFIW4QR9I1o2loFrcrd3o3itCmoo1p7s3lRKe195Ep1elRmU/QhtYSP4tabpaDYrs3I3Ev3yRfV+3Hcfo5yz8LVR+Z5Z96fNUrH8GvO0r/vuwJL82xL1r1tyJTYafepFSehrdQDkJUppSSoZ74OmHS0Ei/okeRH4+9xv50zv7XWkVuvVy5ak7WbjrM6q1B85dlzZC33nD+UtZKj/CdYOloNxtrebeCy6Sig2duteNCpjSlLRCpldlRWEqUcqIbbWEgk9ycd9On6JHkT+Pvcb+dM7+11HWloPWXLl1CW9PnynZMmS4p5555ZW464o5UpSj3USSSSe5J15aWloLZ8avEt5D8doKLaeqDV72uy0W41Krrq1qiHB6fIkA+YhIOPuZ6kYBACSc6ijkFyq3t5NV34X3Uu96VEZcK4VHi5Zp0LPb7kwDjOO3WoqWfao6iPS0C1JUDkHuc5RrQs26rrqVdtWzK5HrdMpkt4OGMtsjLbLigVoQU5HRnoB7hOc5jXS0HQxstzY2H5S2NVmbCuT0K4xSpC5Nt1PpZqDRDJKihOSHkD/DbKgO2ek9tBh4Tclp/FvfmjX4468u3Zx+C7jioyfNp7ih1LCfatpQS4n2ko6fUo6gyDOnUyW1Ppsx+JKYV1tPsOFtxtXvSoYIPzjXhoOi/lduXxoi7B1eBvve1LYtK86SpuKlpwPSZyHEBbTsNpOVOLSehxCgOlJCSSB31z13PdNbuAU+lVC46hVqbbsdVMovpZI9HhB5xxKEoyQgFTi1dIJwVEZOBrAqVZq9ZMY1eqzJxhx24cYyX1O+Sw2MIaR1E9KEjsEjsPYNYegWlpaWg//Z',
     width: 70,
     margin: [20,0,0,5]
     },
		{
			columns: [
				{
          type: 'none',
          lineHeight: 1.5,
					ul: [
						'Academic Director',
					]
				},
				{
          type: 'none',
          lineHeight: 1.5,
					ul: [
						`Hours: ${certificate_key}: 2hours`,
            `Difficulty; ${certificate_key}: Explorer.`
					]
				}
			],
      alignment: 'justify'
		},
	],
	styles: {
    logo: {
      alignment: 'center',
      margin: [0,20,0,40]
    },
		header: {
			fontSize: 22,
			bold: true,
			alignment: 'center',
      margin: [0, 40, 0, 40],
		},
		subheader: {
			fontSize: 15,
			bold: true
		},
		quote: {
			italics: true
		},
		content: {
			fontSize: 16,
      alignment: 'center',
      lineHeight: 1.5,
		},
    footer: {
      // alignment: 'center',
      margin: [25,0]
    }
	}
  };
  pdfMake.createPdf(docDefinition).open();
  // pdfMake.createPdf(docDefinition).download('certificate');

}

// *************  profile page end

// ============= saved userDetails before profile Model =========
function successProfileDetails(title, message) {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'successProfileDetails',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">'+title+'</h2>' +
      '<label>'+message+'</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}
// ============== saved userDetails before profile Model end =========


function saveUserData(save_code) {
  // return true;
  var now = new Date().toISOString()
  if (save_pending || save_code) {
    /// /console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase
      .database()
      .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
      .update({
        user_code: editor.value,
        // last_checkpoint: checkpoint_id,
        //  progress_completed: lesson_progress.progress_completed,
        last_updated: timeNow()
      })
    // ////console.log("Saved code and checkpoint #" + checkpoint_id + " " + timeNow());
    save_pending = false
  } else {
    // console.log("Nothing to save: "+timeNow());
  }
}

function resetUserData() {
  firebase
    .database()
    .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .remove()
}

function resetCurrentProject() {
  resetUserData()
  localStorage.setItem(lessson_url + '_' + auth_id + '_active_slide', 0)
  window.location.reload()
}
// ***********  save user login details to local storage
function saveLogin() {
  // console.log('logindetails//////')
  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var nick_name = document.getElementById('n_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value
  console.log(first_name, last_name, nick_name, day, month, year)
}

function checkpointCompleted(completed_id) {
  firebase
    .database()
    .ref(
      'user_profile/' +
      auth_id +
      '/lesson_progress/' +
      save_lesson_id +
      '/user_checkpoint/' +
       checkpoint_id
    )
    .update({
      completed: true,
      user_code: editor.value,
      completed_at: timeNow()
    })
  checkpoint_completed++

  console.log("checkpoint completed", (checkpoint_completed/checkpoint_count));
  if(checkpoint_completed/checkpoint_count > 0.8){
    console.log("80% checkpoint completed");
    firebase
      .database()
      .ref(
        'users/' +
        auth_id +
        '/certificate/'+
        save_lesson_id)
      .update({
        completed: true,
        user_code: editor.value,
        completed_at: timeNow()
      })
  }

  if (checkpoint_completed === checkpoint_count) {
    lesson_progress.progress_completed = '100'
    lesson_status = 'completed'
    /// /console.log("checkpointCompleted: " + completed_id + " " + timeNow() + " " + checkpoint_completed + " " + checkpoint_count);
  } else {
    lesson_progress.progress_completed = Math.round(
      (checkpoint_id * 100) / (checkpoint_count + 1)
    )
    /// console.log(checkpoint_id + " * 100 / " + checkpoint_count + " = " + lesson_progress.progress_completed);
  }
  ga('send', {
    hitType: 'event',
    eventCategory: lessson_url,
    eventAction: 'Challenge Completed',
    eventLabel: active_slide + 1
  })
  // console.log(active_slide + 1)
  // saveUserData();
}



function updatePreview() {
  var previewFrame = document.getElementById('preview')
  var preview =
    previewFrame.contentDocument || previewFrame.contentWindow.document
  preview.open()
  // var re = /\r/;
  // var reResults = new RegExp(re, "i").test(editor.getValue());
  var check_if_blank = editor.value.replace(/(\r\n\t|\n|\r\t)/gm, '').trim()

  /// /console.log('update preview: "'+check_if_blank+'"');
  if (check_if_blank == '') {
    preview.write(
      '<html><head></head><body><h2 class="no_preview" style="height:100%;width:100%;color:#bbb;font-size:16px;font-family:\'Rajdhani\',sans-serif;text-transform:uppercase;font-weight:bold;position:absolute;top:35%;text-align:center;margin:auto;padding:0;">No preview available<br><br>Time to get coding</h2></body></html>'
    )
    /// /console.log('no preview');
  } else {
    preview.write(editor.value)
  }
  preview.close()

  console.log('Updated Preview')
}

function codeUpdated() {
  save_pending = true
  validateCheckpoint()
  // console.log('Save Pending and Code Validated')
}

function successAnimation() {
  if (active_tab > 0) {
    editor.blur()
    $('.success-animation').removeClass('hide')

    setTimeout(() => {
      $('.success-animation').addClass('hide')
      // console.log('animate2')
      // editor.focus()
    }, 1100)
  }
  // console.log('animate1' + active_tab)
}

function buildSlides() {
  var slide_checkpoint_id = 1
  var slide_indexes = []
  var unlocked_class = ''

  if (
    localStorage.getItem(lessson_url + '_' + auth_id + '_active_slide') !== null
  ) {
    active_slide = localStorage.getItem(
      lessson_url + '_' + auth_id + '_active_slide'
    )
    // console.log(active_slide + "not null");
  } else {
    if (lesson_progress.last_checkpoint > 0) {
      active_slide = slide_indexes[lesson_progress.last_checkpoint - 1] + 1
      // console.log(lesson_progress.last_checkpoint + " null__ " + active_slide);
    } else {
      active_slide = 0
      // console.log(lesson_progress.last_checkpoint+" null__3 "+active_slide);
    }
    // active_slide = lesson_progress.last_checkpoint;
    localStorage.setItem(
      lessson_url + '_' + auth_id + '_active_slide',
      active_slide
    )

    // str = JSON.stringify(slide_indexes, null, 4);
    // console.log("slide_indexes:\n"+str);
  }

  slide_data = lesson_data.slides[active_slide]
  all_slides = lesson_data.slides

  // console.log("active_slide:\n" + active_slide);
  // str = JSON.stringify(slide_data, null, 4);
  // console.log("slide_data:\n" + str);
  // str = JSON.stringify(all_slides, null, 4);
  // console.log("all_slides:\n" + str);

  $.each(all_slides, function (index) {
    /// /console.log(all_slides[index]);

    if (all_slides[index].css_class) {
      custom_class = all_slides[index].css_class
    } else {
      custom_class = ''
    }
    //		console.log("checkpoint: "+index);
    if (all_slides[index].checkpoint === true) {
      // cp_class = " checkpoint";

      if (
        lesson_progress.user_checkpoint !== undefined &&
        lesson_progress.user_checkpoint[slide_checkpoint_id] !== undefined
      ) {
        unlocked_class = ' cp-unlocked'
        // console.log("checkpoint completed: "+slide_checkpoint_id);
        checkpoint_completed++
      } else {
        // console.log("checkpoint not completed " + slide_checkpoint_id);
      }
      checkpoint_count++
      slide_indexes.push(index)
    } else {
      // cp_class = "";
      unlocked_class = ''
    }

    // $("#lesson-page .swiper-wrapper").append("<div class=\"swiper-slide chal" + slide_checkpoint_id + " " + unlocked_class + " \" id=\"slide" + (index + 1) + "\">\n" + all_slides[index].html_content + "\n</div>");
    $('#lesson-page .swiper-wrapper').append(
      '<div class="swiper-slide ' +
      custom_class +
      unlocked_class +
      ' " id="slide' +
      (index + 1) +
      '">\n' +
      all_slides[index].html_content +
      '\n</div>'
    )

    if (all_slides[index].onload_function !== undefined) {
      /// /console.log('onload_function triggered: ' + all_slides[index].onload_function);
      var func = new Function(all_slides[index].onload_function)
      func()
    }

    if (all_slides[index].checkpoint === true) {
      $(
        '#lesson-page .swiper-wrapper .swiper-slide:nth-child(' +
        (index + 1) +
        ')'
      ).attr('data-checkpoint_id', slide_checkpoint_id++)
      // console.log("cp_index: "+slide_checkpoint_id);
    }
  })

  // console.log("checkpoint_count: "+checkpoint_count);
  // $( "#lesson-page .swiper-wrapper" )
  // $('#lesson-page .swiper-wrapper .swiper-slide:nth-child(10)').addClass('active');
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index])
    })
  }
}

function unlockSkills(skills) {
  // console.log("skills: "+skills);
  $('#' + skills + '-skill').addClass('has-skill-true')
  $('#' + skills + '-skill').removeClass('has-skill-false')
  // console.log('Unlocked skill '+skills);
  // console.log("#"+slide_id+"-skill");

  $('#' + skills + '-skill')
    .attr(
      'data-original-title',
      $('#' + skills + '-skill').attr('data-title-text')
    )
    .tooltip('show')
    .tooltip('hide')
}


function validateCheckpoint() {
  // slide_data = lesson_data.slides[active_slide-1];
  // active_slide = swiperV.activeIndex+1;
  // console.log('Trigger validation' + slide_data.reg)
  str = JSON.stringify(slide_data, null, 4)
  // console.log("slide_data:\n" + str);

  slide_id = active_slide + 1
  /* if (slide_data.checkpoint === true) {
    if (!$("#slide" + slide_id).hasClass('cp-unlocked')) {

    }
  } */
  if (slide_data.action === true) {
    // ////console.log('action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
    // console.log(
    //   'action set for slide | slide_data action: ' +
    //     slide_data.action +
    //     ' | slide_id: ' +
    //     slide_id
    // )
    if (slide_data.reg !== undefined) {
      var cp_unlocked = false
      // do something
      // console.log('Run validation on ' + active_slide)

      $.each(slide_data.reg, function (index, re) {
        // console.log('#slide' + slide_id + ' .task-' + (index + 1))
        // var editorValue = editor.value;
        // var trimSpace = editorValue.replace(/\s/gm,"");
        // console.log(trimSpace,'TS')
        var reResults = new RegExp(re, 'i').test(editor.value)
        console.log('Run validation results ' + reResults,re)
        //	$(".check-info").removeClass('d-none');
        if (reResults) {
          cp_unlocked = true
          if (
            !$('#slide' + slide_id + ' .task-' + (index + 1)).hasClass(
              'correct'
            )
          ) {
            $('#slide' + slide_id + ' .task-' + (index + 1)).addClass('correct')
            saveUserData(true)
          }
          // console.log(index)
          if (
            !$('#slide' + slide_id + ' .task-' + (index + 1) + ' i').hasClass(
              'icon-beenhere'
            )
          ) {
            $('.success-animation').addClass('hide')
            successAnimation()

            // $("#slide" + slide_id + " li:eq( " + index + " ) ").css( "color", "red" );
            /// /console.log("added correct class " + "#slide" + slide_id + " .task-" + (index + 1) + " i");
          }
          // console.log(
          //   '#slide' + slide_id + ' li:eq( ' + index + ' ) .check-icon'
          // )
          $(
            '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
          ).html('<i class="icon-beenhere"></i>')
          // $("#slide" + slide_id + " .actions li").addClass('correct');
          // $(".check-info").addClass('correct');
          // else { //else used so don't save data twice
          // }

          // $(".check-info .check-icon").html('<i class="icon-close1"></i>');

          // $(".pagination .next").addClass('disabled');
          // $(".pagination .next").removeClass('disabled');
          /// /console.log('Run validation using: ' + re);
          // $(".success-animation").show();

          // console.log('checkpoint_id: '+checkpoint_id);
        } else {
          $(
            '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
          ).html('<i class="icon-close1"></i>')
          $('#slide' + slide_id + ' .task-' + (index + 1)).removeClass(
            'correct'
          )
          // $(".check-info .check-icon").html('<i class="icon-close1"></i>');
          // $(".check-info").removeClass('correct');
          // $(".pagination .next").addClass('disabled');
        }
      })

      if (cp_unlocked) {
        /// /console.log("#slide" + slide_id + " cp_unlocked");
      } else {
        /// /console.log("#slide" + slide_id + " cp_locked");
      }

      if (!$('#slide' + slide_id).hasClass('cp-unlocked') && cp_unlocked) {
        checkpoint_id = $('#slide' + slide_id).data('checkpoint_id')
        $('#slide' + slide_id).addClass('cp-unlocked')
        checkpoint_id = slide_data.checkpoint_id;
        checkpointCompleted(checkpoint_id)

        if (slide_data.unlock_skills !== undefined) {
          unlockSkills(slide_data.unlock_skills)

          if (user_skills !== null) {
            user_skills.push(slide_data.unlock_skills)
            var unique_user_skills = []
            $.each(user_skills, function (i, el) {
              if ($.inArray(el, unique_user_skills) === -1) {
                unique_user_skills.push(el)
              }
            })
            /// /console.log("set unique unlockSkills: " + unique_user_skills);
            firebase
              .database()
              .ref('/user_profile/' + auth_id + '/skills')
              .set(unique_user_skills)
          } else {
            user_skills = {
              '0': slide_data.unlock_skills
            }
            /// /console.log("set unlockSkills: " + user_skills);
            firebase
              .database()
              .ref('/user_profile/' + auth_id + '/skills')
              .set(user_skills)
          }
        }
      } /*  */
    }
  } else {
    /// /console.log('no action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
  }

  if (slide_data.js_function !== null) {
    /// /console.log('js_function trigger: ' + slide_data.js_function);
    // var func = new Function("console.log('i am a new function')");
    var func = new Function(slide_data.js_function)
    func()
  }
}

function progressUpdate() {
  var progressPercentage = checkpoint_count / checkpoint_id
  firebase
    .database()
    .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .set({
      progressCompleted: progressPercentage
    })
}

function inactivityTime() {
  var t
  window.onload = resetTimer
  document.onload = resetTimer
  document.onmousemove = resetTimer
  document.onmousedown = resetTimer // touchscreen presses
  document.ontouchstart = resetTimer
  document.onclick = resetTimer // touchpad clicks
  document.onscroll = resetTimer // scrolling with arrow keys
  document.onkeypress = resetTimer

  /// /console.log("inactivityTime");
  resetTimer()

  var resumeCoding = [
    'You snooze, you loose.',
    'Well begun is only half done',
    'You are on your way to becoming a coding genius',
    'Did you know when you complete the lesson you will get a certificate?'
  ]
  var rindex = Math.floor(Math.random() * resumeCoding.length)

  function timedOut() {
    /*
         if ( !$(".modal:visible").length ) {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
             popupStyle: 'speech-bubble '+current_avatar+' surprised',
            htmlContent:
            '<div id=\"confirm_dialog\" style=\"display: block;\">'+
            '\t<h2 id=\"confirm_title\">'+resumeCoding[rindex]+'</h2>'+
            '\t<div id=\"confirm_actions\">'+
            '\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s continue coding</button>'+
            '\t</div>'+
            '</div>'
    });
  } */
  }

  function resetTimer() {
    // console.log("r timer: "+$(".modal").length);
    clearTimeout(t)
    t = setTimeout(timedOut, 60000)

    // ////console.log("didn't reset timer"+$(".modal").attr('style').display == 'block' ));
  }
}

function editorChange() {
  clearTimeout(delay)
  // console.log('preview');
  delay = setTimeout(updatePreview, 100)
  setTimeout(codeUpdated, 200)
}
function closeModal(options) {
  /// /console.log("close modal: " + options);
  $(options).modal('hide')
}

function onStart() {

  // initCodeEditor("< > /  =");
  if (lesson_id === "P001-T02-M-V008") {
    editor.value = '<head>\n</head>\n<body>\n<h1>\n</h1>\n</body>'
  } else {
    editor.value = lesson_progress.user_code
  }

  buildSlides()
  /// /console.log("start" + active_slide);
  // saveUserData(true);
  // Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip()

  // Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    var $target = $(e.target)

    if ($target.parent().hasClass('disabled')) {
      return false
    }
  })


  setInterval(saveUserData, 1000)

  // Live preview

  // editor.on('change', function () {
  //   clearTimeout(delay);
  //   console.log('preview');
  //   delay = setTimeout(codeUpdated, 500);
  // });
  var lessonLoaded = false

  // initFullPage();

  $('#fp-nav ul li:nth-child(2) a span').addClass('cp-green')
  $('#fp-nav ul li:nth-child(12) a span').addClass('cp-red')

  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowToXuchMove: false,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () { },
      slideChange: function () {
        var slider = this
        active_tab = slider.activeIndex
        $('#lesson-navbar li.active').removeClass('active')
        var mindex = slider.activeIndex + 1

        $('#lesson-navbar li:nth-child(' + mindex + ')').addClass('active')
        // console.log('#lesson-navbar:nth-child('+mindex+')');
        if (slider.activeIndex <= 0) {
          $('.success-animation').addClass('')
        }
        if (mindex === 2) {
          if (greetcode === false) {
            greetingsCode().delay(1000);
          }
          $('.bottom_keys').removeClass('hide')
          
        } else {
          $('.bottom_keys').addClass('hide')
        }
        if (mindex === 3) {
          slider.allowSlideNext = true
          if (greetview === false) {
            greetingsView().delay(1000);
          }
        }
        if (slider.activeIndex <= 3) {
         
        } else {
          slider.allowSlideNext = false
        }
        // console.log("tab: "+active_tab);
      },
      transitionEnd: function () {
        var slider = this
        if (slider.activeIndex === 2) {
          updatePreview()
        }
        if (slider.activeIndex === 1) {
          $('.CodeMirror-code').focus()
          editor.focus()
          // $('.ui-keyboard').show();
          // console.log('show')
        } else {
          $('.CodeMirror-code').blur()
          // $('.ui-keyboard').hide();
          // console.log('hide')
        }
      }
    }
  })
  swiperV = new Swiper('.swiper-container-v', {
    direction: 'vertical',
    spaceBetween: 50,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    preloadImages: false,
    // initialSlide: active_slide,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true
    },
    pagination: {
      el: '.swiper-pagination-v',
      clickable: false
    },
    observer: true,
    observeParents: true,

    on: {
      init: function () { },
      lazyImageReady: function () {
        if (slide_data.lazy_function !== undefined) {
          var func = new Function(slide_data.lazy_function)
          func()
        }
        /// /console.log('lazy_function triggered: ' + slide_data.lazy_function);
      },
      slideChange: function () {
        var slider = this
        // var mindex = slider.activeIndex+1;
        active_slide = slider.activeIndex
        localStorage.setItem(
          lessson_url + '_' + auth_id + '_active_slide',
          active_slide
        )
        slide_data = lesson_data.slides[active_slide]
        // console.log(slide_data)
        var unlocked = $('#slide' + (active_slide + 1)).hasClass('cp-unlocked')
        if (!unlocked && slide_data.reg !== undefined) {
          slider.allowSlideNext = false
          /// /console.log('lock' + slider.allowSlideNext + active_slide);
        } else {
          slider.allowSlideNext = true
          /// /console.log('unlock' + active_slide);
        }
        // console.log("mindex: "+mindex);
        /// /console.log("active_slide: " + active_slide);

        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
          .prevAll()
          .addClass('bullet-green')
        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
          .next()
          .removeClass('bullet-green')
        validateCheckpoint()
        saveUserData()
        // ga('send', 'pageview', 'CJ Lesson', 'swipe', lessson_url, active_slide + 1);
        ga('send', {
          hitType: 'event',
          eventCategory: lessson_url,
          eventAction: 'Slide swipe',
          eventLabel: active_slide + 1
        })
      }
    }
  })

  swiperTabs = document.querySelector('.swiper-container-h').swiper
  swiperLesson = document.querySelector('.swiper-container-v').swiper

  swiperH.allowTouchMove = false
  swiperV.allowTouchMove = false

  $('#lesson-navbar li').click(function () {
    if ($(this).index() === 6) {
      /// /console.log(swiperH.activeIndex);
      // ////console.log($(this).index());
      swiperH.allowSlideNext = true
      swiperH.slideTo($(this).index())
      swiperH.allowSlideNext = false
    } else {
      // console.log("dfd"+swiperH.activeIndex);
      // ////console.log("dfd"+$(this).index());
      swiperH.slideTo($(this).index())
      $('#lesson-navbar li.active').removeClass('active')
      $(this).addClass('active')
    }
  })
  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1)
    $('.swiper-pagination-switch').removeClass('active')
    $(this).addClass('active')
  })
  $('.swiper-next').click(function () {
    swiperLesson.allowSlideNext = true
    swiperLesson.slideNext()
    // swiperLesson.allowSlideNext = false;
    /// /console.log("skip");
  })
  $('.swiper-editor').click(function () {
    swiperTabs.allowSlideNext = true
    swiperTabs.slideNext()
    // swiperTabs.allowSlideNext = false;
    /// /console.log("skip2");
  })
  swiperLesson.slideTo(active_slide)
  // active_slide = 0;

  $('.reset-profile').click(function () {
    resetCurrentProject()
  })

  // $("#profile_page .profile_name").append(", " + user_profile.first_name);

  $('.reset-lesson').click(function () {
    $('.modal ').remove()
    // console.log('Came here to reset the lesson')
    $.createDialog({
      modalName: 'reset-lesson',
      popupStyle: 'speech-bubble ' + current_avatar + ' scared',
      actionButton: resetCurrentProject,
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">Are you sure you want to reset this lesson?</h2>' +
        '\t<div id="confirm_actions">' +
        '\t\t<button id="close_button" class="btn btn-primary light cancel" datax-dismiss="modal"><a href="#" rel="modal:close">Hmmm, maybe not</a></button>' +
        '\t\t<button id="action_button" class="btn btn-primary light action reset-profile">Yes</button>' +
        '\t</div>' +
        '</div>'
    })
  })


  var i
  var current_year = new Date().getFullYear()
  function pad2(number) {
    return (number < 10 ? '0' : '') + number
  }

  date_form =
    '<form lpformnum="1" id="email-login"><div ><input class="form-control" id="f_name" placeholder="First name" name="fname" type="text" required><input class="form-control" id="l_name" placeholder="Last name" name="lname" type="text" required=""><input class="form-control" id="n_name" placeholder="Nickname (Username)" name="nickname" type="text" required=""></div><div><label style="    margin: 0;">Date of birth</label></div>\n' +
    '<select name="day" id="B-day" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n'
  for (i = 1; i <= 31; i++) {
    date_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
  }
  date_form +=
    '</select>\n' +
    '<select name="month" id="B-month" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n' +
    '<option value="01">Jan</option>\n' +
    '<option value="02">Feb</option>\n' +
    '<option value="03">Mar</option>\n' +
    '<option value="04">Apr</option>\n' +
    '<option value="05">May</option>\n' +
    '<option value="06">June</option>\n' +
    '<option value="07">July</option>\n' +
    '<option value="08">Aug</option>\n' +
    '<option value="09">Sept</option>\n' +
    '<option value="10">Oct</option>\n' +
    '<option value="11">Nov</option>\n' +
    '<option value="12">Dec</option>\n' +
    '</select>\n' +
    '<select name="year" id="B-year" class="custom-select" style="width: 38%">\n' +
    '<option value="" selected></option>\n'
  for (i = current_year; i >= 1950; i--) {
    date_form += '<option value="' + i + '">' + i + '</option>\n'
  }
  date_form += '</select>\n'
  date_form += '</div><div id="error"></div></form>\n'

  $('.login-button').click(function () {
    loginModal()
  })

  $('.register-button').click(function () {
    registerModal()
  })

  $('.select_lego').click(function () {
    changeAvatar('lego')
  })
  $('.select_robot').click(function () {
    changeAvatar('robot')
  })

  // updatePreview();

  var twitterShare = document.querySelector('[data-js="twitter-share"]')

  twitterShare.onclick = function (e) {
    e.preventDefault()
    var twitterWindow = window.open(
      'https://twitter.com/share?url=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'twitter-popup',
      'height=350,width=600'
    )
    if (twitterWindow.focus) {
      twitterWindow.focus()
    }
    return false
  }

  var facebookShare = document.querySelector('[data-js="facebook-share"]')

  facebookShare.onclick = function (e) {
    e.preventDefault()
    var facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'facebook-popup',
      'height=350,width=600'
    )
    if (facebookWindow.focus) {
      facebookWindow.focus()
    }
    return false
  }
  $('#whatsapp,#whatsapp2').attr(
    'href',
    'whatsapp://send?text=Check out the website I coded on CodeJIKA 😎‏%0A‎👉 ' +
    'https://www.codejika.com/preview/' +
    save_lesson_id +
    '?' +
    auth_id
  )

  $('#gallery').click(function () {
    // console.log('clicked gallery')
    // loginModal();
    addToGallery()
  })

  $('.loading').hide()
  swiperH.allowTouchMove = true
  swiperV.allowTouchMove = true
  inactivityTime()


  $('html head')
    .find('title')
    .text(lesson_data.pageTitle)
  // console.log("lesson_data.pageTitle: " + lesson_data.pageTitle)
  $('#description').attr('content', lesson_data.pageDesc)
  $('#keywords').attr('content', lesson_data.pageKeywords)
}

// ************* end of onStart()

function loginModal() {
  $('.modal ').remove();
  $(".blocker").css("display",'none')
  $.createDialog({
    modalName: 'login-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: saveLogin,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Type your name and date of birth to login.</h2>' +
      date_form +
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button login-button" class="btn btn-encouraging light action" type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })

  // close modal on save
  $('.action').click(function () {
    var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    // var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    console.log(first_name, last_name, day, month, year)

    if (first_name == '') {
      document.querySelector('#f_name', '').classList.add('error-border')
      return false
    }
    else if (last_name == '') {
      document.querySelector('#l_name', '').classList.add('error-border')
      return false
    } else {

            // store to users storage
        // var ref = firebase.database().ref('/users/' + auth_id)
        var ref = firebase.database().ref('/users/')
        ref.orderByChild("first_name").equalTo(first_name).once('value').then((snapshot)=> {
          data = snapshot.val()
          var matched = false;
          if(data){
            Object.keys(data).forEach(function(k){
              var in_data = data[k];
              Fname = in_data.hasOwnProperty('first_name') ? in_data['first_name'] : '';
              Lname = in_data.hasOwnProperty('last_name') ? in_data['last_name'] : '';
              v_day = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('day') ? in_data.D_O_B['day'] : '' : '';
              v_month = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('month') ? in_data.D_O_B['month'] : '' : '';
              v_year = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('year') ? in_data.D_O_B['year'] : '' : '';
              console.log(Fname, Lname, v_day, v_month, v_year, "From firebase");
              console.log(first_name, last_name, day, month, year, "Inputs");

              if(Fname == first_name && Lname == last_name && v_day == day && v_month == month && v_year == year ){
                 $('#login-modal').modal('hide')
                $('.modal , .modal-backdrop').remove()
                $('.blocker').css('display', 'none');
                successProfileDetails('Login Successfull', 'Welcome back ' + Fname)
                matched = true;
                console.log("Came inside");
                localStorage.setItem('userid', k)
                auth_id = k
                var newurl =
                  window.location.protocol +
                  '//' +
                  window.location.host +
                  window.location.pathname +
                  '?' +
                  auth_id
                console.log('newurl: ' + newurl)
                window.history.pushState({ path: newurl }, '', newurl)
                getCurrentSlideNumber();
                checkUserKey()
                // break;
              }

          });
          }
          

          if(! matched){
            alert('Invalid credentials')
          }
        })

    }

  })
}

function drawArrow(start_id, end_id, dynamicAnchors, acolor = 'red-line') {
  jsPlumb.ready(function () {
    jsPlumb.setContainer('container')
    var link = jsPlumb.getInstance()

    link.connect({
      cssClass: acolor,
      source: start_id,
      target: end_id,
      anchors: dynamicAnchors,
      endpoint: 'Blank',
      overlays: [
        [
          'Arrow',
          {
            location: 1,
            width: 10,
            length: 10
          }
        ]
      ],
      paintstyle: {
        lineWidth: 40,
        strokeStyle: 'red'
        // dashstyle:" 0 1"
      },
      connector: [
        'Bezier',
        {
          curviness: 80
        }
      ]
    })
  })
}
/***************Greetings ******************/

function greetingsCode(){
    // console.log("reg");
    localStorage.setItem('greetcode', true)
    refreshValues();
    $('.modal ').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">GUESS WHAT?!? :)</h2>' +
        
        '\t<p>This is the Code Editor.'+'<br>'+' Where the magic happens.</p><br>Swipe back to keep learning. ' +'<br>'+'<br>'+'<br>'+
        '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Got it!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
    })
  }
  function greetingsView(){
    // console.log("reg");
    localStorage.setItem('greetview', true)
    refreshValues();
    $('.modal ').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">CHECK YOUR WORK.</h2>' +
        
        '\t<p> This is the Web Viewer.<br> Where you get to check on what you'+"'"+'re building. </p>' +'Swipe back twice to keep learning.'+'<br>'+'<br>'+'<br>'+
        '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Nice!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
    })
  }
/******************************************* */

function registerModal() {
  // console.log("reg");
  $('.modal ').remove()
  $.createDialog({
    modalName: 'register-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    actionButton: registerNextModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Registration is soooo easy. We just need a few details to get started</h2>' +
      date_form +
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Sign up">' +
      '\t</div>' +
      '<p class="sign-up login-button">Already have an account ? <span class="a-link">Login</span></p>' +
      '</div>'
  })
  $('.login-button').click(function () {
    loginModal()
  })
}

function registerNextModal() {

    var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    // console.log(first_name, last_name, nick_name, day, month, year)

    // store to users storage
    // var ref = firebase.database().ref('/users')
    var ref = firebase.database().ref('/users/');
    firebase.database().ref('/users/'+auth_id).once('value').then(function (snapshot) {
        data = snapshot.val()
        console.log("data", data);
        var updateLoginDetails = data;
        if(data){
          var keys = Object.keys(data)
          updateLoginDetails['first_name'] = first_name
          updateLoginDetails['last_name'] = last_name
          updateLoginDetails['nick_name'] = nick_name
          updateLoginDetails['D_O_B'] = { day, month, year }
        }else{
          updateLoginDetails = {
            first_name: first_name,
            last_name: last_name,
            nick_name: nick_name,
            D_O_B: { day, month, year }
          }
        }
        console.log("updateLoginDetails", updateLoginDetails);
        ref.update({
          [auth_id]: updateLoginDetails
        })
        // return snapshot.val();
    })




  if (first_name == '' ) {
    document.querySelector('#f_name', '').classList.add('error-border')
    return false
  }
  if(last_name == ''){
     document.querySelector('#l_name', '').classList.add('error-border')
    return false
  }
  if (nick_name == '') {
    document.querySelector('#n_name', '').classList.add('error-border')
    return false
  } else {
    var register_addtional =
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Congrats you have successfullly registered.</h2>' +
      '<p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>' +
      '<form lpformnum="1" id="email-login"><div >\n' +
      '<input class="form-control" id="mobile_number" placeholder="Mobile number" name="mobile" type="text" required="" style="width: 100%">\n' +
      '<input class="form-control" id="email_id" placeholder="Email address" name="email" type="email" required="" style="width: 100%">\n' +
      '<input class="form-control" id="id_number" placeholder="ID number" name="id_number" type="text" required="" style="width: 100%">\n' +
      '<select name="country" id="eCountry" class="custom-select" style="width: 44%">\n' +
      '<option value="" selected>Country</option>\n' +
      '<option value="South Africa">South Africa</option>\n' +
      '<option value="Mozambique">Mozambique</option>\n' +
      '<option value="Nambia">Nambia</option>\n' +
      '<option value="Zambia">Zambia</option>\n' +
      '<option value="Zimbabwe">Zimbabwe</option>\n' +
      '</select>\n' +
      '<input class="form-control school" id="shcoolName" placeholder="Name of School" name="school" type="text" required="" style="width: 54%">\n' +
      '</div><div id="error"></div></form>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light cancel" datax-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Save">' +
      '\t</div>' +
      '</div>'
  }

  $('#register-modal .modal-body').html(register_addtional)

  $('#close_button, .modal-backdrop').click(function () {
    $('#register-modal').modal('hide')
    $('.modal , .modal-backdrop').remove()
    $('.blocker').css('display', 'none')
    // console.log('closed #register-modal')
  })
  $('previewIcon').click(function () {
    $('.modal , .modal-backdrop').remove()
  })

  $('.action').click(function () {

    var mobile_number = document.getElementById('mobile_number').value
    var email_id = document.getElementById('email_id').value
    var id_number = document.getElementById('id_number').value
    var country = document.getElementById('eCountry').value
    var school = document.getElementById('shcoolName').value
    // console.log(first_name, last_name, nick_name, day, month, year)

    if (country == '') {
      document.querySelector('#eCountry').classList.add('error-border')
    }
    if (school == '') {
      document.querySelector('#shcoolName').classList.add('error-border')
    }
    else {
      document.querySelector('#eCountry').classList.remove('error-border');
       document.querySelector('#shcoolName').classList.remove('error-border');
      // store to users storage
    var ref = firebase.database().ref('/users/' + auth_id )

    var additionalDetails = {
      mobile_number: mobile_number,
      email_id: email_id,
      id_number: id_number,
      country: country,
      school_name: school
    }
    ref.update({
      details: additionalDetails
    }).then(function (snapshot){
      successProfileDetails('Signup Successfull', 'Welcome Aboard')
    // console.log(additionalDetails, 'Details')
    })
    }

  })
}

function changeAvatar(avatar) {
  $('.modal-content')
    .removeClass(current_avatar)
    .addClass(avatar)
  $('.select_robot, .select_lego').toggleClass(
    'option_selected option_unselected'
  )
  current_avatar = avatar
  successAnimation()
}

// ***************** title popup in home page ************
function titleModal() {
  // console.log("reg");
  $('.modal ').remove()
  $.createDialog({
    modalName: 'title-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    actionButton: titleInput,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Awesome</h2>' +
      '<form titleForm="1" id="title-form"><div ><label>What do you want to call your project?<span class="required-mark">*</span></label><input class="form-control" id="project_name" placeholder="Super Code Stretch-man  v1" name="Pname" type="text" required=""><div><label style="    margin: 0;">Do you have any notes? (Optional)</label></div><textarea class="form-control" id="project_notes" placeholder="This project  reminds me of the pet spider I had in 2ndgrade, that.." name="nickname" type="text" cols="4"></textarea></div></form>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action projectTitle" type="submit" value="Save">' +
      '\t</div>' +
      '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })

}
// ************** title popup end ==============

// ============= unRegistered Model =========
function UnRegisteredModal() {
  // console.log("reg");
  $('.modal ').remove()
  $(".blocker").css("display",'none')
  $.createDialog({
    modalName: 'unRegistered-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad',
    actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">OOOOPS!</h2>' +
      '<label>This feature is only available for logged in users.</label>\n\n\n' +
      '<h4>Login Now</h4>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action " type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Not Registered Yet? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'
  })
  $('.register-button').click(function () {
    registerModal()
  })
}
// ============== unRegistered Model end =========

// ============= saved your code Model =========
function savedToGallery() {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'projectSaved-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Yeahhhh!</h2>' +
      '<label>Your Project is Saved...</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}
// ============== saved your code Model end =========

function initFirebase() {
  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyAw269vPfE3QreRGZDuEisv3wSnfFmFFoY',
    authDomain: 'codejika-staging.firebaseapp.com',
    databaseURL: 'https://cj-staging.firebaseio.com/',
    projectId: 'codejika-staging',
    storageBucket: 'codejika-staging.appspot.com',
    messagingSenderId: '405485160215'
  }

  firebase.initializeApp(config)
}

function getLessonDataPromise() {
  return firebase
    .database()
    .ref('/lessons/' + lesson_id)
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
        str = JSON.stringify(lesson_data, null, 4)
        // console.log('lesson_data:\n' + str)
      },
      function (error) {
        console.log(error)
      }
    )
  console.log('getLessonDataPromise')
}

function getUserProfilePromise() {
  return firebase
    .database()
    .ref('/user_profile/' + auth_id + '/profile')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        /// /console.log(error);
      }
    )
  /// /console.log("getUserProfilePromise");
}

function getUserSkillsPromise() {
  return firebase
    .database()
    .ref('/user_profile/' + auth_id + '/skills')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        /// /console.log(error);
      }
    )
  /// /console.log("getUserSkillsPromise");
}

function getLessonProgressPromise(default_code) {
  return firebase
    .database()
    .ref('/gallery/' + auth_id + '/5-min-website/')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        console.log(error);
      }
    )
  /// /console.log("getLessonProgressPromise");
}
// Gallery Code starts here
// var loading = document.getElementById('loadingIcon')
// loading.style.display = 'none'

var current_page = 0
var page_id_array = ['']

function addToGallery() {
  var userCode, keyString
  var check_if_blank = editor.value.replace(/(\r\n\t|\n|\r\t)/gm, '').trim()
  if (check_if_blank == '') {
    alert('write some code')
    return false
  }

  var validateAuthId = firebase.database()

  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function (snapshot) {
      // loading.style.display = "block";
      data = snapshot.val()
      var keys = Object.keys(data)
      var validateName = keys['first_name']
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
      // console.log(keyString, 'userDetails')
      console.log(auth_id, 'auid')
      if (findKey == auth_id) {
        titleModal()
      } else if (findKey !== auth_id || keys == null) {
        UnRegisteredModal()
        // alert("Your code is added to the gallery");
      }
    }) // end of firebase response validAuth2
}

function go_next() {
  // console.log(page_id_array, 'here is the array')
  current_page += 1
  loading.style.display = 'block'
  document.getElementById('galleryContainer').innerHTML = loading
  $('#bottom-pagination').css('visibility', 'hidden')
  $('#previous2').css('visibility', 'hidden')
  userCodeHtml2 = ''
  getGalleryData(page_id_array[current_page], '')
  document.getElementById('pageNumber').innerHTML = 1 + current_page
  document.getElementById('pageNumber2').innerHTML = 1 + current_page

  if (current_page != 0) {
    document.getElementById('previous').style.visibility = 'visible'
    document.getElementById('previous2').style.visibility = 'visible'
  }
}

function go_previous() {
  if (current_page != 0) {
    current_page -= 1
    loading.style.display = 'block'
    document.getElementById('galleryContainer').innerHTML = loading
    $('#bottom-pagination').css('visibility', 'hidden')
    userCodeHtml2 = ''
    getGalleryData(page_id_array[current_page], '')
    document.getElementById('pageNumber').innerHTML = 1 + current_page
    document.getElementById('pageNumber2').innerHTML = 1 + current_page
  }

  // Hide Previous button
  if (current_page == 0) {
    $('#previous').css('visibility', 'hidden')
    $('#previous2').css('visibility', 'hidden')
  }
}

var user_HTML
var current_key1
var user_code1
var userCodeHtml2 = ''
var pagesize = 6
var current_page_size = 0
function getGalleryData(start_with_id, filter) {
  firebase_object = firebase
    .database()
    .ref('/gallery/5-min-website')
    .orderByKey()
  if (start_with_id && start_with_id != '') {
    // console.log(start_with_id, 'Here is last fetched key')
    firebase_object.startAt(start_with_id)
  }
  var lastVisible = ''
  console.log('Here is gallery')
  firebase_object
    .limitToFirst(pagesize * (current_page + 1))
    .once('value')
    .then(
      function (snapshot) {
        // console.log(pagesize * (current_page + 1), 'hello')
        var data = snapshot.val()
        var keys = Object.keys(data)
        if (keys.length < pagesize * (current_page + 1)) {
          console.log('Here')
          document.getElementById('next2').style.visibility = 'hidden'
        } else {
          console.log('Here2')
          document.getElementById('next2').style.visibility = 'visible'
        }

        // console.log(keys, 'Here are the keys ')
        var sorted_array = []
        for (i = 0; i < keys.length; i++) {
          var k = keys[i]
          // dataLikes = data[k].date
          data[k]['key'] = k
          sorted_array.push(data[k])
          // console.log(likeArray,'data&k')
        }
        // console.log(sorted_array, 'before SortArray')
        if (filter && filter != '') {
          sorted_array.sort(function (a, b) {
            if (filter == 'MostLiked') {
              return b.likes - a.likes
            } else if (filter == 'Recent') {
              return new Date(b.date) - new Date(a.date)
            }
            return 0
          })
        }
        // console.log(sorted_array, 'After SortArray')
        current_page_size = sorted_array.length
        for (i = 0; i < sorted_array.length; i++) {
          loading.style.display = 'none'
          var key = sorted_array[i]['key']
          $('#bottom-pagination').css('visibility', 'visible')
          if (i < pagesize * current_page) {
            continue
          }

          var current_key1 = key
          // var userCodeURL =
          //   "https://www.codejika.com/preview/5-minute-website?" + current_key;
          var name =
            data[current_key1].first_name + ' ' + data[current_key1].last_name
          var likes = data[current_key1].likes
          user_HTML = escape(data[current_key1].user_code)
          var user_code1 = escape(data[current_key1].user_code)
          var title = data[current_key1].title;
          var notes = data[current_key1].notes
          // console.log(user_HTML,'user')
          userCodeHtml2 += `
          <div id = "userKey" >
              <div class="card-blog" id="cardHTML">
              <div  class ="priview_blog"><a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')">
                <h3>${title}</h3>
                <a onclick="openthumbnail('${current_key1}', '${user_HTML}', '${user_code1}')" id="previewIcon">
                 View
                 <span><i class="icon-enlarge" style="font-family: 'icomoon' !important;font-size: inherit;margin-left: 4px;vertical-align: -2px"></i></span>
                </a>
              </div>
              <a  class="HTMLscreen" >
              <iframe src="data:text/html;charset=utf-8, ${user_code1}"  scrolling="no" id="my_iframe_${current_key1}">
              </iframe style="background:#fff">
            </a>

              <div class="detail-blog">
              <h4>${name}</h4>

            <p id="book_${current_key1}" class="text-center mb-0">${notes}</p>

              <div class="d-flex view-section">
                <div class="likes">
                    <button onclick="likes('${current_key1}')"><span><i class="icon-thumb_up_alt new-icon"  style="font-family: 'icomoon' !important;"></i></button><small id='my_like_${current_key1}'>${likes}</small></span>
                </div>
                <div class="views">
                </div>
              </div>
              </div>
            </div>

          </div>`
          // console.log(userCodeHtml2, "Here is the code");
          lastVisible = current_key1
        }
        if (dialogInMenu) {
          document.getElementById('galleryContainer').innerHTML = userCodeHtml2
          // return userCodeHtml2
        }
        // galleryPageJS = userCodeHtml2
        $( "#book_"+ current_key1 ).hide();

        $("#clickme_"+current_key1).click(function() {
          // console.log('toggle')
          $( "#book_"+ current_key1 ).slideToggle();
        });
        page_id_array.push(lastVisible)
        return snapshot.val()
      },
      function (error) {
        console.log(error, 'galleryError')
      }
    )

  return firebase_object
}
$(document).ready(function () {
  successAnimation()
  // ================= jquery Modal
  $(function () {
    $('#viewHTML').click(function () {
      // takes the ID of appropriate dialogue
      var id = $(this).data('#sub-modal')
      // open dialogue
      $(id).dialog('open')
    })
  })
  initFirebase()
  var getLessonData = getLessonDataPromise()
  var getUserProfile = getUserProfilePromise()
  // var getUserSkills = getUserSkillsPromise();  P0 disabled
  var getLessonProgress = getLessonProgressPromise()
  getCurrentSlideNumber()
  // var getGallery = getGalleryData('', '')

  // Promise.all([getLessonData, getUserProfile, getUserSkills, getLessonProgress]).then(function (results) {  P0 disabled
  Promise.all([
    getLessonData,
    getUserProfile,
    getLessonProgress,
    // getGallery
  ]).then(function (results) {
    lesson_data = results[0]
    // str = JSON.stringify(lesson_data, null, 4);
    // console.log("lesson_data:\n"+str);

    user_profile = results[1]
    // str = JSON.stringify(user_profile, null, 4);
    // console.log("user_profile:\n"+str);

    // user_skills = results[2]; P0 disabled
    // str = JSON.stringify(user_skills, null, 4);
    // console.log("user_skills:\n"+str);

    lesson_progress = results[2]

    // if no lesson data save default values to DB
    if (lesson_progress === null) {
      /// /console.log("no lesson progress found");
      lesson_progress = {
        last_checkpoint: '0',
        user_code: lesson_data.defaultCode,
        progress_completed: '0'
      }
      // saveUserData(true);
      /// /console.log("default lesson_progress inserted");
    }

    // str = JSON.stringify(lesson_progress, null, 4);
    // console.log("default lesson_progress:\n"+str);

    /// /console.log("promise all is true, now running onStart");
    onStart()
  })

  $('#disallow-interact,#allow-interact').click(function () {
    $('#disallow-interact.icon-locked').toggleClass('hide')
    $('#allow-interact.icon-unlocked').toggleClass('hide')
    $('.swipe-overlay').toggleClass('pointer-none')
  })

  $('.swipe-overlay').click(function () {
    // $( "#disallow-interact.icon-locked" ).toggleClass( "hide" )
    // $( "#allow-interact.icon-unlocked" ).toggleClass( "hide" )
    // $( ".swipe-overlay" ).toggleClass( "pointer-none" )
    // console.log("test");
  })

  // TLN
  TLN.append_line_numbers('editor')

  hideButton()
  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

// function to display register modal
  // setTimeout(checkUserKey, 5000);
  checkUserKey()
})
//  ============ end of document.ready firebaseinit()

function checkUserKey(){
  let userName = document.querySelector('.profile_name')
  var validateAuthId = firebase.database()
  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function (snapshot) {
      data = snapshot.val()
      var keys = Object.keys(data)
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
       if (findKey != auth_id) {
        // registerModal()
      } else{
        validateAuthId.ref('/users/' + auth_id).once('value').then((snapshot)=>{res = snapshot.val()
          // var uKeys = Object.keys(res);
          var uFname = res.first_name;
          var uLname = res.last_name
          userName.innerText ='Hello, '+uFname;

        })
      }
    }
  )
}

function likes(user_id) {
  // console.log("I am inside likes");

  like_count_ui = document.getElementById('my_like_' + user_id)
  var new_likes = parseInt(like_count_ui.innerHTML) + 1
  var ref = firebase.database().ref('/gallery/5-min-website/' + user_id)

  ref.once('value').then(function (snapshot) {
    // console.log(snapshot.val(), "Here os the snapsho");

    if (snapshot.val() && snapshot.val().likes) {
      new_likes = snapshot.val().likes + 1
    }
    like_count_ui.innerHTML = new_likes
    ref.update({
      likes: new_likes
    })
  })
}

function hideButton() {
  if (current_page == 0) {
    // document.getElementById('previous').style.visibility = 'hidden'
    // document.getElementById('previous2').style.visibility = 'hidden'
  }
}

function FilterData(event) {
  var selectedOption = document.getElementById('selectOptions').value
  // console.log(selectedOption, 'option')
  userCodeHtml2 = ''
  $('#loadingIcon').css('display', 'block')
  $('.pagination-section').css('visibility', 'visible')
  // console.log('iam in projects')
  var previousPage = document.getElementById('galleryLikes')
  previousPage.style.display = 'none'
  var previousPage2 = document.getElementById('galleryContainer')
  previousPage2.innerHTML = getGalleryData('', selectedOption)
}

$('.projectTitle').click(function () {
  titleInput()
})
function titleInput() {
  var title = document.getElementById('project_name').value
  var projectNotes = document.getElementById('project_notes').value
  if (title == '') {
    document.querySelector('#project_name').classList.add('error-border')
  } else {
    // update DB gallery
    var user_firstName
    var user_lasttName

    var getDetails = firebase.database()
    getDetails
      .ref('/users/' + auth_id)
      .once('value')
      .then(function (snapshot) {
        user = snapshot.val()
        // console.log(user, 'firstname to gal')
        var toupdate = {
          first_name: user.first_name,
          last_name: user.last_name,
          likes: 0,
          user_code: editor.value,
          title: title,
          notes: projectNotes,
          date: new Date()
        }
        var ref = firebase.database().ref('/gallery/5-min-website')
        ref.update({
          [auth_id]: toupdate
        })
      })
    savedToGallery()

    userCodeHtml2 = ''
    setTimeout(getGalleryData('', ''), 300)

    document.querySelector('#project_name').classList.remove('error-border')
    $('.modal , .modal-backdrop').remove()
    $('.blocker').css('display', 'none')
    // console.log(title, projectNotes)
  }
}

// ============= thumbnail Model =========
function viewThumbnail(clicked_key, user_html_code, user_code_output) {
  // $('.modal ').remove()
  subModal = true;
  $.nestedModal1({
    modalName: 'viewThumbnail-modal',
    htmlContent: `<div >
        <div class="popup-header">
          <a id="viewHTML" class="view-code">VIEW</a>
        </div>
        <iframe src="data:text/html;charset=utf-8, ${user_code_output}" style="width:100%; height: 100vh;overflow:hidden; border: none;" scrolling="yes" id="my_iframe_${clicked_key}">

        </iframe>
      </div>
      `,
      closeExisting:false
  })
  $('.view-code').click(function () {
    outputHTML1(clicked_key, user_html_code, user_code_output);
    // $('#viewThumbnail').modal({
    //   closeExisting:true
    // })
  })
}
// ============== thumbnail Model end =========

function openthumbnail(clicked_key, user_html_code, user_code_output) {
  // console.log(clicked_key, user_html_code, user_code_output, 'Here is my key')
  viewThumbnail(clicked_key, user_html_code, user_code_output)
  subModal = true;
}

function outputHTML1(clicked_key, user_html_code, user_code_output) {
  user_html_code = unescape(user_html_code)
  // $('.modal ').remove()
  $.nestedModal1({
    modalName: 'outputHTML1-modal',
    // popupStyle: "speech-bubble top-align " + current_avatar + " happy",
    // actionButton: resetCurrentProject,
    htmlContent: `

        <div style="height:100vh;overflow:hidden;">
        <div class="popup-header">
          <a class="view-output view1_button" >OUTPUT</a>
        </div>
          <div><xmp id=view_html_code_${clicked_key} class="add-background">${user_html_code}</xmp></div>
        </div>
    `
  })
  $('.view1_button').click(function () {
    // viewThumbnail(clicked_key)
    viewThumbnail(clicked_key, user_html_code, user_code_output)
    // $('#viewThumbnail-modal').modal({
    //   closeExisting:false
    // })
  })
}

function p1t1Output() {
  $.galleryDialog({
    modalName: 'P1T1-output',
    htmlContent: `
    <header class="p1t1-header">
		<div class="p1t1-div">
			<h1 class="p1t1-h1">Jeff  Molefe</h1>
			<h3 class="p1t1-h3"> Launching soon... </h3>
			<br><p>18th June, 2022</p>
		</div>
	</header>
	<section class="p1t1-section">
	<div class="p1t1-div">
		<h3 class="p1t1-h3">
				MOTIVATION:
				<i class="p1t1-i"><br><br>
				I want to learn how to code so that I can...
				[ ...build cool websites, learn more about tech...]
				</i>
		</h3>
	</div>
	</section>
 <footer class="p1t1-footer" style="padding-bottom:40px;">
  <div class="p1t1-div" >
  <p>&copy 2021 Jeff Molefe</p>
  </div>
 </footer>
    `
  })
}

// ///////////////  backButton functionality
var timeout;
history.pushState(null, null, location.href);
window.onpopstate = function () {
  history.go(1);

  clearTimeout(timeout);
  timeout = setTimeout(function () {
    if (swiperTabs.activeIndex == 0) {
      swiperLesson.slideTo(swiperLesson.activeIndex - 1);
      console.log("on lesson tab" + swiperLesson.activeIndex);
    }
    if (swiperTabs.activeIndex == 3 && dialogInMenu) {
      // alert('clicked Back button')
      $('.blocker').trigger('click')
      dialogInMenu = false
      console.log("on lesson tab" + swiperLesson.activeIndex);
      return
    }
    if (swiperTabs.activeIndex == 3 && subModal){
       $('.blocker').trigger('click')
       subModal = false
      // return
    } else {
      return swiperTabs.slideTo(swiperTabs.activeIndex - 1);
      // console.log("on other tab" + swiperTabs.activeIndex);
    }

  }, 50);


};

//Check if user is loggedin or not
function checkLoggedInAndGetUserDetails(){
  let promise = new Promise(function(resolve, reject){
    firebase.database()
      .ref('/users/' + auth_id)
      .once('value')
      .then(function (snapshot) {
        console.log("Database searched",auth_id );
        user = snapshot.val()
        if(user){
          console.log("Database searched resolve");
          resolve(user)
        }else{
          console.log("Database searched reject");
          reject()
        }
      })

  })
  return promise;
}
