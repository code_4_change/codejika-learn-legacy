/*!

 =========================================================
 * Now-ui-kit - v1.1.0
 =========================================================

 * Product Page: https://www.creative-tim.com/product/now-ui-kit
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md)

 * Designed by www.invisionapp.com Coded by www.creative-tim.com

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */

var transparent = false;

var transparentDemo = false;
var fixedTop = true;

var navbar_initialized,
    backgroundOrange = false,
    toggle_initialized = false;

	
var showPopover = function () {
    $(this).tooltip('show');
};
var hidePopover = function () {
    $(this).tooltip('hide');
};
function updateCountdown() {
    // 140 is the max message length
    var remaining = 140 - jQuery('.message').val().length;
    jQuery('.countdown').text(remaining + ' characters remaining.');
}
	
  
 

var start = new Date();
var maxTime = 20000;
var seconds = maxTime/1000;
var timeoutVal = Math.floor(maxTime/200);
var pause = false;
var color1 = "51,141,185";
var color2 = "109,179,242";

function animateUpdate() {
    var now = new Date();
    var timeDiff = now.getTime() - start.getTime();
    var perc = Math.round((timeDiff/maxTime)*100);
   console.log(perc);
      if (perc <= 100 && !pause) {
     //  if ( isInViewport('#pbar_bgcolor')) {
         updateProgress(perc);
     //  }
       setTimeout(animateUpdate, timeoutVal);
      }
      if (perc == 100) { 
      //alert('Too slow... redirecting to Lesson 01'); 
      $('#pbar_bgcolor').text("[ loading lesson data ]");
      $('#pbar_bgcolor').addClass("blink-redirect");
      $('#pbar_pause').hide();
      setTimeout(function() {
          //  window.location.assign("https://codejika.co.za/projects/learn-html-css-javascript-01.htm");
        }, 550);
      
      }
}

function updateProgress(percentage) {
   $('#countdown').text("in \"" + (seconds - Math.round((percentage*seconds)/100)) + "\" seconds");
   var percentage = 100 - percentage;
    $('#pbar_bgcolor').css("background", " -moz-linear-gradient(left, rgba(" + color1 + ",1) 0%, rgba(84,163,238,1) " + percentage + "%, rgba(" + color2 + ",1) " +( percentage +1) + "%, rgba(" + color2 + ",1) 100%)");
        $('#pbar_bgcolor').css("background", " -webkit-linear-gradient(left, rgba(" + color1 + ",1) 0%, rgba(84,163,238,1) " + percentage + "%, rgba(" + color2 + ",1) " +( percentage +1) + "%, rgba(" + color2 + ",1) 100%)");
        
        $('#pbar_bgcolor').css("background", "linear-gradient(to right, rgba(" + color1 + ",1) 0%,rgba(84,163,238,1) " + percentage + "%,rgba(" + color2 + ",1) " +( percentage +1) + "%, rgba(" + color2 + ",1) 100%)");
    
}

function isInViewport(checkThis) {
  var elementTop = $(checkThis).offset().top;
  var elementBottom = elementTop + $(checkThis).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  if ( elementBottom > viewportTop && elementTop < viewportBottom) {
console.log("true" + elementBottom + " " + viewportTop + " "  + elementTop + " "  + viewportBottom);   
   return true;
    
  } else {
    console.log("false" + elementBottom + " " + viewportTop + " "  + elementTop + " "  + viewportBottom); 

    return false;
  }
}

$(document).ready(function() {
  
/*  animateUpdate();
    $('#pbar_pause').click(function() {
       
      pause = true;
      //updateProgress(100);
      $('#countdown').text("");
      $('#pbar_bgcolor').text("GO NOW");
      $('#pbar_pause').hide();
    });
  
  jQuery(document).ready(function($) {
    updateCountdown();
    $('.message').change(updateCountdown);
    $('.message').keyup(updateCountdown);
}); */

    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		html: true,
		trigger: 'manual'
}).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
	   $('#'+$(this).attr('id')+' .skills-example').clone().appendTo('.skill-details-content');
    });

//$('#email-input-skill').tooltip('show'); // Force show tooltip on page load to debug

/*
      var delay;
      // Initialize CodeMirror editor with a nice html5 canvas demo.
      var editor = CodeMirror.fromTextArea(document.getElementById('code'), {
		theme: 'base16-dark',
		mode: "htmlmixed",
		lineNumbers: true,
		lineWrapping: true,
		gutters: ["CodeMirror-lint-markers"],
		lint: true
      });
      editor.on("change", function() {
        clearTimeout(delay);
        delay = setTimeout(updatePreview, 300);
      });
      
      function updatePreview() {
        var previewFrame = document.getElementById('preview');
        var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
        preview.open();
        preview.write(editor.getValue());
        preview.close();
      }
      setTimeout(updatePreview, 300);
*/

    // Activate Popovers and set color for popovers
    $('[data-toggle="popover"]').each(function() {
        color_class = $(this).data('color');
        $(this).popover({
            template: '<div class="popover popover-' + color_class + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        });
    });


    $('.form-control').on("focus", function() {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function() {
        $(this).parent(".input-group").removeClass("input-group-focus");
    });

    // Activate bootstrapSwitch
    $('.bootstrap-switch').each(function() {
        $this = $(this);
        data_on_label = $this.data('on-label') || '';
        data_off_label = $this.data('off-label') || '';

        $this.bootstrapSwitch({
            onText: data_on_label,
            offText: data_off_label
        });
    });
    
     $('.modal-show').click(function(){
               var dataURL = $(this).attr('data-href');
            $($(this).attr('data-target')+' .modal-content').load(dataURL,function(){
              $('.modal-content').append('<button type="button" class="close" data-dismiss="modal">&times;</button>')
            });
        
    });

   // if ($(window).width() >= 992) {
      //  big_image = $('.page-header-image[data-parallax="true"]');

        //$(window).on('scroll', nowuiKitDemo.checkScrollForParallax);
   // }

   //$('#download-section').focus();
   /*   $('#menu-modal').click(function() {
    
        showPopup('#menu-modal');
    
      });*/
      $('.slide-to[data-slide-to]').on('click', function(){
        var slide_num = $(this).data('slide-to');
    $('#carouselExampleControls').carousel(slide_num);
    console.log(slide_num);
});
});
/*
function showPopup(popupID) {
    
        $(popupID).show();

        $('#close_topright').click(function() {
        $(popupID).hide();
        $('body,html').css('overflow', 'initial'); 
        $('.modal , .modal-backdrop').remove(); 
        });
    console.log(popupID);

   
}



var big_image;



// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
};
*/
