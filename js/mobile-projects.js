var lesson_id = "P001-L01-M-V001";
var save_lesson_id = "P001-L01";
// var auth_id = "my7n4sM81zdnhxHzirawc4wOX9M2";
var auth_id = localStorage.getItem('userid')


var delay;
var lesson_data = null;
var lesson_progress = null;
var slide_data = null;
var user_skills = null;
var active_slide;
var active_tab;
var save_pending = false;
var checkpoint_id = 0;
var checkpoint_count = 0;
var checkpoint_completed = 0;
var progressPercentage;
var current_avatar = "robot";
var swiperTabs;
var swiperLesson;

var DEBUG = true;
// var dateBase = firebase.database()

if (!DEBUG) {
  if (!window.console) window.console = {};
  var methods = ["log", "debug", "warn", "info"];
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () {};
  }
}


function timeNow() {
  return new Date().toISOString();
}

function saveUserData(save_code) {
  var now = new Date().toISOString();
  if (save_pending || save_code) {
    console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).update({
      user_code: editor.getValue(),
      last_checkpoint: checkpoint_id,
      progress_completed: lesson_progress.progress_completed,
      last_updated: timeNow()
    });
    console.log("Saved code and checkpoint #" + checkpoint_id + " " + timeNow());
    save_pending = false;
  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}

function resetUserData() {
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).remove();

}

function resetCurrentProject() {
  resetUserData();
  localStorage.setItem("active_slide", 0);
  window.location.reload();
}

function successAnimation() {
if (active_tab > 0) {
		$(".success-animation").removeClass('hide');

		setTimeout(function () {
			$(".success-animation").addClass('hide');
			console.log('animate2');
		}, 2500);
	}
		console.log('animate1'+active_tab);
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index]);
    });
  }

}

function unlockSkills(skills) {
  //console.log("skills: "+skills);
  $("#" + skills + "-skill").addClass('has-skill-true');
  $("#" + skills + "-skill").removeClass('has-skill-false');
  //console.log('Unlocked skill '+skills);
  //console.log("#"+slide_id+"-skill");

  $("#" + skills + "-skill").attr('data-original-title', $("#" + skills + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
}

function inactivityTime() {
  var t;
  window.onload = resetTimer;
  document.onload = resetTimer;
  document.onmousemove = resetTimer;
  document.onmousedown = resetTimer; // touchscreen presses
  document.ontouchstart = resetTimer;
  document.onclick = resetTimer; // touchpad clicks
  document.onscroll = resetTimer; // scrolling with arrow keys
  document.onkeypress = resetTimer;

  console.log("inactivityTime");
  resetTimer();

	var resumeCoding = [
		"You snooze, you loose.",
		"Well begun is only half done",
		"You are on your way to becoming a coding genius",
		"Did you know when you complete the lesson you will get a certificate?"
	]
	var rindex = Math.floor(Math.random() * resumeCoding.length); 
	
  function timedOut() {
	if ( !$(".modal:visible").length ) {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
 			popupStyle: 'speech-bubble '+current_avatar+' surprised', 
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">'+resumeCoding[rindex]+'</h2>'+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s continue coding</button>'+
			'\t</div>'+
			'</div>'	
    });
	}
  }
	
  function resetTimer() {

      //console.log("r timer: "+$(".modal").length);
      clearTimeout(t);
      t = setTimeout(timedOut, 120000)
     
		     // console.log("didn't reset timer"+$(".modal").attr('style').display == 'block' ));
  }
}

function closeModal(options) {
  console.log("close modal: " + options);
  $(options).modal('hide');
}

function onStart() {

  //  Activate the Tooltips
  $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
    html: true,
    trigger: 'manual'
  }).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function () {
    $('#' + $(this).attr('id') + '.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
  });
  loadSkills();


  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowToXuchMove: false,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true,
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () {

      },
      slideChange: function () {
        var slider = this;
				active_tab = slider.activeIndex;
        $('#lesson-navbar li.active').removeClass('active');
        var mindex = slider.activeIndex + 1;

        $('#lesson-navbar li:nth-child(' + mindex + ')').addClass('active');
        //console.log('#lesson-navbar:nth-child('+mindex+')');

        if (slider.activeIndex <= 0) {
          $(".success-animation").addClass('hide');
        }
        if (slider.activeIndex <= 2) {
          slider.allowSlideNext = true;
        } else {
          slider.allowSlideNext = false;
        }
          //console.log("tab: "+active_tab);			  
      },
    }

  });

  /*swiperV = new Swiper('.swiper-container-v', {
    direction: 'vertical',
    spaceBetween: 0,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    preloadImages: false,
    //initialSlide: active_slide,
    // Enable lazy loading
    lazy: true,
    paXgination: {
      el: '.swiper-pagination-v',
      clickable: false,
    },
    observer: true,
    observeParents: true,
	freeMode: true,
	scrollbar: {
	el: '.swiper-scrollbar',
	},

    on: {
      init: function () {

      },
      slideChange: function () {
        var slider = this;
       
        active_slide = slider.activeIndex;

        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').prevAll().addClass('bullet-green');
        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').next().removeClass('bullet-green');

      },
    }
  });*/
  

  swiperTabs = document.querySelector('.swiper-container-h').swiper;
  //swiperLesson = document.querySelector('.swiper-container-v').swiper;
  
  swiperH.allowSlidePrev = false;
  swiperH.allowSlideNext = false;
	  
  $('#lesson-navbar li').click(function () {
      swiperH.allowSlidePrev = true;
      swiperH.allowSlideNext = true;
      swiperH.slideTo($(this).index());
      swiperH.allowSlidePrev = false;
      swiperH.allowSlideNext = false;
  })
  
  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1);
    $('.swiper-pagination-switch').removeClass('active');
    $(this).addClass('active')
  })
 /* $('.swiper-next').click(function () {
    swiperLesson.allowSlideNext = true;
    swiperLesson.slideNext();
    //swiperLesson.allowSlideNext = false;
    console.log("skip");
  })*/
  $('.swiper-editor').click(function () {
    swiperTabs.allowSlideNext = true;
    swiperTabs.slideNext();
    //swiperTabs.allowSlideNext = false;
    console.log("skip2");
  })
  //swiperLesson.slideTo(active_slide);
  //active_slide = 0;

  $('.reset-profile').click(function () {
    resetCurrentProject();
  });

  // $("#profile_page .profile_name").append(", " + user_profile.first_name);

	if (!localStorage.getItem("skipProjIntro") ) {
	
	    $.createDialog({
      modalName: 'project-intro',
 			popupStyle: 'speech-bubble '+current_avatar+' surprised', 
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">So are you ready to get coding?</h2>'+
			'\t<p id=\"\">Here you will find our \"Intro to HTML\" course that consists of 3 projects. </p>'+
			'\t<div id=\"confirm_actions\">'+
			'\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s start coding</button>'+ '<br>' +
			'<label class=\"form-check-label\"><input id=\"skip-proj-intro\" type=\"checkbox\" class=\"form-check-input\">Don\'t show again</label>'+			
			'\t</div>'+
			'</div>'	
    });
	    $('#skip-proj-intro').click(function(){
			if ($('#skip-proj-intro').is(':checked')) {
				localStorage.setItem("skipProjIntro", 1) ;
			} else {
				localStorage.removeItem("skipProjIntro") ;
			}
		});

	}

	

		

	var i
  var current_year = new Date().getFullYear()
  function pad2(number) {
    return (number < 10 ? '0' : '') + number
  }

  date_form =
    '<form lpformnum="1" id="email-login"><div ><input class="form-control" id="f_name" placeholder="First name" name="fname" type="text" required><input class="form-control" id="l_name" placeholder="Last name" name="lname" type="text" required=""><input class="form-control" id="n_name" placeholder="Nickname (Username)" name="nickname" type="text" required=""></div><div><label style="    margin: 0;">Date of birth</label></div>\n' +
    '<select name="day" id="B-day" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n'
  for (i = 1; i <= 31; i++) {
    date_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
  }
  date_form +=
    '</select>\n' +
    '<select name="month" id="B-month" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n' +
    '<option value="01">Jan</option>\n' +
    '<option value="02">Feb</option>\n' +
    '<option value="03">Mar</option>\n' +
    '<option value="04">Apr</option>\n' +
    '<option value="05">May</option>\n' +
    '<option value="06">June</option>\n' +
    '<option value="07">July</option>\n' +
    '<option value="08">Aug</option>\n' +
    '<option value="09">Sept</option>\n' +
    '<option value="10">Oct</option>\n' +
    '<option value="11">Nov</option>\n' +
    '<option value="12">Dec</option>\n' +
    '</select>\n' +
    '<select name="year" id="B-year" class="custom-select" style="width: 38%">\n' +
    '<option value="" selected></option>\n'
  for (i = current_year; i >= 1950; i--) {
    date_form += '<option value="' + i + '">' + i + '</option>\n'
  }
  date_form += '</select>\n'
  date_form += '</div><div id="error"></div></form>\n'

	$('.login-button').click(function () {
    loginModal();
  });
	
	$('.register-button').click(function () {
		registerModal()
  });

    $(".select_lego").click(function(){changeAvatar("lego")});
	$(".select_robot").click(function(){changeAvatar("robot")});
	
	$(".project-view").click(function(){openProject($(this).data("project-id"))});
	$(".project-return").click(function(){closeProject($(this).data("project-id"))});


	inactivityTime();
	
	
	$(".lessons li").each(function() {
		var s = $(this).data("progress-completed") + '% 100%';
		// This will get 's' as 'n% 100%'. We have to only change the width, 
		// height remains 100%. We assign this 's' to the css.
		$(this).css({"background-size": s});
	});
	
	if(window.location.hash) {
		var hash = window.location.hash.substring(1);		
		var re = new RegExp("^p([1-9]|[1-9][0-9])-lesson([1-9]|[1-9][0-9])$");
		if (re.test(hash)) {
			var p_id = hash.split("-", 1)[0].substring(1);
			var l_id = hash.split("-", 2)[1].substring(6);
			console.log("Valid"+p_id+" "+l_id);
			  swiperLesson.slideTo(p_id-1);
			  openProject("project-"+l_id);
		}
		
	}
}

var current_project_id;

function openProject(project_id) {
	current_project_id = project_id;
	$("#"+project_id).removeClass('hide');
	//$(".swiper-container-v").addClass('hide');
	$(".list_projects").addClass('hide');
	$('#lesson-navbar li:nth-child(1) span').html('PROJECT '+(project_id.split("-").pop()));

}

function closeProject(project_id) {
	$("#"+current_project_id).addClass('hide');
	//$(".swiper-container-v").removeClass('hide');	
	$(".list_projects").removeClass('hide');	
	$('#lesson-navbar li:nth-child(1) span').html('PROJECTS');
	current_project_id = null;	
}
	
function loginModal() {
	$('.modal ').remove(); 
	$.createDialog({
      modalName: 'login-modal',
			popupStyle: 'speech-bubble top-align '+current_avatar+' happy', 
      //actionButton: resetCurrentProject,
			htmlContent: 
			'<div id=\"confirm_dialog\" style=\"display: block;\">'+
			'\t<h2 id=\"confirm_title\">Please enter your details below to login</h2>'+
			date_form+
      '\t<div id="confirm_actions">' +
      '\t\t<input id="action_button login-button" class="btn btn-encouraging light action" type="submit" value="Login">' +
      '\t</div>' +
      '<p class="sign-up register-button">Dont have an account? <span class="a-link register-button">Sign Up</span></p>' +
      '</div>'					
    });
		$('.register-button').click(function () {
			registerModal()
		});

    $('.action').click(function () {
    var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    // var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    console.log(first_name, last_name, day, month, year)

    if (first_name == '') {
      document.querySelector('#f_name', '').classList.add('error-border')
      return false
    }
    else if (last_name == '') {
      document.querySelector('#l_name', '').classList.add('error-border')
      return false
    } else {
      document.querySelector('#f_name', '').classList.remove('error-border')
      document.querySelector('#l_name', '').classList.remove('error-border')
            // store to users storage
        // var ref = firebase.database().ref('/users/' + auth_id)
        var ref = firebase.database().ref('/users/')
        ref.orderByChild("first_name").equalTo(first_name).once('value').then((snapshot)=> {
          data = snapshot.val()
          var matched = false;
          Object.keys(data).forEach(function(k){
              var in_data = data[k];
              Fname = in_data.hasOwnProperty('first_name') ? in_data['first_name'] : '';
              Lname = in_data.hasOwnProperty('last_name') ? in_data['last_name'] : '';
              v_day = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('day') ? in_data.D_O_B['day'] : '' : '';
              v_month = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('month') ? in_data.D_O_B['month'] : '' : '';
              v_year = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('year') ? in_data.D_O_B['year'] : '' : '';
              console.log(Fname, Lname, v_day, v_month, v_year, "From firebase");
              console.log(first_name, last_name, day, month, year, "Inputs");

              if(Fname == first_name && Lname == last_name && v_day == day && v_month == month && v_year == year ){
                 $('#login-modal').modal('hide')
                $('.modal , .modal-backdrop').remove()
                $('.blocker').css('display', 'none');
                successProfileDetails('Login Successfull', 'Welcome back ' + Fname);
                $("#profile_page .profile_name").append(", " + first_name);

                matched = true;
                console.log("Came inside");
                localStorage.setItem('userid', k)
                auth_id = k
                var newurl =
                  window.location.protocol +
                  '//' +
                  window.location.host +
                  window.location.pathname +
                  '?' +
                  auth_id
                console.log('newurl: ' + newurl)
                window.history.pushState({ path: newurl }, '', newurl)
                getCurrentSlideNumber()

                // break;
              }

          });

          if(! matched){
            alert('Invalid credentials')
          }
        })

    }

  })
}

function registerModal() {
	//console.log("reg");
	$('.modal ').remove(); 
	$.createDialog({
		modalName: 'register-modal',
		popupStyle: 'speech-bubble top-align '+current_avatar+' happy', 
		actionButton: registerNextModal,
		htmlContent: 
		'<div id=\"confirm_dialog\" style=\"display: block;\">'+
		'\t<h2 id=\"confirm_title\">Registration is soooo easy. We just need a few details to get started</h2>'+
		date_form+
		'\t<div id="confirm_actions">' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Sign up">' +
      '\t</div>' +
      '<p class="sign-up login-button">Already have an account ? <span class="a-link">Login</span></p>' +
      '</div>'					
	});
	$('.login-button').click(function () {
		loginModal();
	});
}

function registerNextModal() {
  var first_name = document.getElementById('f_name').value
    var last_name = document.getElementById('l_name').value
    var nick_name = document.getElementById('n_name').value
    var day = document.getElementById('B-day').value
    var month = document.getElementById('B-month').value
    var year = document.getElementById('B-year').value
    console.log(first_name, last_name, nick_name, day, month, year)
	console.log("add code here to save user and then proceed to collect addtional data");

  var ref = firebase.database().ref('/users')

    var updateLoginDetails = {
      first_name: first_name,
      last_name: last_name,
      nick_name: nick_name,
      D_O_B: { day, month, year }
    }
    ref.update({
      [auth_id]: updateLoginDetails
    })
    if (first_name == '' ) {
    document.querySelector('#f_name', '').classList.add('error-border')
    return false
  }
  if(last_name == ''){
     document.querySelector('#l_name', '').classList.add('error-border')
    return false
  }
  if (nick_name == '') {
    document.querySelector('#n_name', '').classList.add('error-border')
    return false
  }
  else {
    var register_addtional =
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Congrats you have successfullly registered.</h2>' +
      '<p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>' +
      '<form lpformnum="1" id="email-login"><div >\n' +
      '<input class="form-control" id="mobile_number" placeholder="Mobile number" name="mobile" type="text" required="" style="width: 100%">\n' +
      '<input class="form-control" id="email_id" placeholder="Email address" name="email" type="email" required="" style="width: 100%">\n' +
      '<input class="form-control" id="id_number" placeholder="ID number" name="id_number" type="text" required="" style="width: 100%">\n' +
      '<select name="country" id="eCountry" class="custom-select" style="width: 44%">\n' +
      '<option value="" selected>Country</option>\n' +
      '<option value="South Africa">South Africa</option>\n' +
      '<option value="Mozambique">Mozambique</option>\n' +
      '<option value="Nambia">Nambia</option>\n' +
      '<option value="Zambia">Zambia</option>\n' +
      '<option value="Zimbabwe">Zimbabwe</option>\n' +
      '</select>\n' +
      '<input class="form-control school" id="shcoolName" placeholder="Name of School" name="school" type="text" required="" style="width: 54%">\n' +
      '</div><div id="error"></div></form>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light cancel" datax-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Save">' +
      '\t</div>' +
      '</div>'	
  }
	
	$('#register-modal .modal-body').html(register_addtional);
	
	$('#close_button, .modal-backdrop').click(function () {
		$('#register-modal').modal('hide'); 
		$('.modal , .modal-backdrop').remove(); 
		console.log('closed #register-modal');
	});
   $('.action').click(function () {

    var mobile_number = document.getElementById('mobile_number').value
    var email_id = document.getElementById('email_id').value
    var id_number = document.getElementById('id_number').value
    var country = document.getElementById('eCountry').value
    var school = document.getElementById('shcoolName').value
    // console.log(first_name, last_name, nick_name, day, month, year)

    if (country == '') {
      document.querySelector('#eCountry').classList.add('error-border')
      return
    }
    if (school == '') {
      document.querySelector('#shcoolName').classList.add('error-border')

    }
    else {
      document.querySelector('#eCountry').classList.remove('error-border');
       document.querySelector('#shcoolName').classList.remove('error-border');
      // store to users storage
    var ref = firebase.database().ref('/users/' + auth_id )

    var additionalDetails = {
      mobile_number: mobile_number,
      email_id: email_id,
      id_number: id_number,
      country: country,
      school_name: school
    }
    ref.update({
      details: additionalDetails
    }).then(function (snapshot){
      successProfileDetails('Signup Successfull', 'Welcome Aboard')
    // console.log(additionalDetails, 'Details')
    })
    }

  })
  
}

function changeAvatar(avatar) {
	$(".modal-content").removeClass(current_avatar).addClass(avatar);
	$(".select_robot, .select_lego").toggleClass("option_selected option_unselected");
	current_avatar = avatar;
	successAnimation();
}


function initFirebase() {
  // Initialize Firebase
  var config = {
    // production firebase credentials

    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
// ******************************************************************
    // staging firebase credentials

    // apiKey: 'AIzaSyAw269vPfE3QreRGZDuEisv3wSnfFmFFoY',
    // authDomain: 'codejika-staging.firebaseapp.com',
    // databaseURL: 'https://cj-staging.firebaseio.com/',
    // projectId: 'codejika-staging',
    // storageBucket: 'codejika-staging.appspot.com',
    // messagingSenderId: '405485160215'
  }

  firebase.initializeApp(config);
}

function getLessonDataPromise() {

  return firebase.database().ref('/lessons/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonDataPromise");
}

function getUserProfilePromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/profile').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserProfilePromise");
}

function getUserSkillsPromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/skills').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserSkillsPromise");
}

function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}



$(document).ready(function () {

// console.log(localStorage.getItem('userid'),'hello');
console.log(auth_id,'authID');


  initFirebase();

  var getUserProfile = getUserProfilePromise();
  var getUserSkills = getUserSkillsPromise();
  var getLessonProgress = getLessonProgressPromise();
  getCurrentSlideNumber()

  Promise.all([getUserProfile, getUserSkills, getLessonProgress]).then(function (results) {

    user_profile = results[0];
    //str = JSON.stringify(user_profile, null, 4);
    //console.log("user_profile:\n"+str); 

    user_skills = results[1];
    str = JSON.stringify(user_skills, null, 4);
    console.log("user_skills:\n"+str); 

    lesson_progress = results[2];


    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str); 

    console.log("promise all triggered, now running onStart");
    onStart();
  });



});
// ---------------------- end of document.ready function --------------

// ============= saved userDetails before profile Model =========
function successProfileDetails(title, message) {
  $('.modal ').remove()
  $.createDialog({
    modalName: 'successProfileDetails',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent:
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">'+title+'</h2>' +
      '<label>'+message+'</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-encouraging light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}

// Database continuity
var lesson_mappings = {
    "coding1-t1": "P1Training1",
    "coding1-t2": "P1Training2",
    "lesson-m": "P1Training1",
    "P1Training1": "P1Training1",
    "P1Training2": "P1Training1",
}
function getCurrentSlideNumber() {
    var current_lesson = window.location.href;
    console.log(current_lesson)
    for (var key in lesson_mappings) {
        console.log(key)
        console.log(current_lesson);
        console.log(current_lesson.includes(key))
        if (current_lesson.includes(key)) {
            var lesson_id = lesson_mappings[key];
            console.log(lesson_id, 'lesson_id');
            firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/').once('value').then(function (snapshot) {
                data = snapshot.val()
                if(data){
                  var keys = Object.keys(data)
                  var k = keys[Object.keys(data).length - 1]
                  if(data[k].user_code){
                    editor.value = data[k].user_code
                  }
                }

                // return snapshot.val();
            })
        }
    }
}