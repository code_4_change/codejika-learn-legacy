// CodeMirror HTMLHint Integration
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [], message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for ( var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity : message.type
      });
    }
    return found;
  }); 
});

// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

var delay;

// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',  
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});

// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});
function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
	
	validateCheckPoint();
	/*if (preg_match('%(<p[^>]*>.*?</p>)%i', $subject, $regs)) {
    $result = $regs[1];
} else {
    $result = "";
}*/
//console.log($("#editor").text().match('<p[^>]*>.*?</p>'));

//$("div:match('/[^a-zA-Z0-9]/')")
}


function checkIfTagExists(src, tag, betweenTag) {
   if (!betweenTag) { betweenTag = "(.*?)";}
   
   var re = "<"+tag+"[^>]*>\s*"+betweenTag+"\s*<\\/"+tag+">";
   

	//	console.log(re);
		//console.log(re2);
		
    return new RegExp(re).test(src);
}

function validateCheckPoint() {
	
	var checkPoints = {
		12: "(<h1>|<h1 [^>]*>)\s*(.*?)\s*<\/h1>",
		14: "(<p>|<p [^>]*>)\s*(.*?)\s*<\/p>",
		19: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>)",
		23: "(<input( \s*|[^]* )\s*type\s*=\s*\"\s*submit\s*\"\s*[^>]*>)",
		27: "(?=(<input( \s*|[^]* )\s*type\s*=\s*\"\s*email\s*\"\s*[^>]*>))(?=(<input( \s*|[^]* )\s*placeholder\s*=\s*\"\s*Your\s* \s*email\s*\"\s*[^>]*>))",
		219: "<h2 [^>]*>\s*Hi\s*Tembi\s*<\\/h2>",
		318: "^\s*Date\s*\(\s*\)\s*$"
	}

	if( checkPoints[currentStep] !== undefined ) {
    // do something
		//console.log('Run validation only on '+currentStep);
		var re = checkPoints[currentStep];
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		//console.log('Run validation results '+reResults);
			$(".check-info").removeClass('d-none');			
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
			$("#slide"+currentStep+" .actions li").addClass('correct');
			$(".check-info").addClass('correct');			
			$(".check-info .check-icon").html('/');			
						$(".pagination .next").addClass('disabled');
			//$(".pagination .next").removeClass('disabled');
		//			console.log('Run validation using '+checkPoints[currentStep]);
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".check-info .check-icon").html('\\');
			$(".check-info").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	} else {
		$(".check-info").addClass('d-none');
	}
	
	var unlockSkills = {
		12: "h1-h6",
		15: "p",
		20: "email-input",
		24: "submit-input"
	}

	if( unlockSkills[currentStep] !== undefined ) {
				$("#"+unlockSkills[currentStep]+"-skill").addClass('has-skill-true');
				$("#"+unlockSkills[currentStep]+"-skill").removeClass('has-skill-false');
		//console.log('Unlocked skill '+unlockSkills[currentStep]);
	//	console.log("#"+currentStep+"-skill");

	  $("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title', $("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+unlockSkills[currentStep]+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  //	  console.log("#"+unlockSkills[currentStep]+"-skill");
	  //console.log($("#"+unlockSkills[currentStep]+"-skill").attr('data-title-text'));
	}	
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);





setTimeout(updatePreview, 300);

var lessonProgressBar = document.getElementById('lessonProgressBar');
var maxNum = lessonProgressBar.dataset.valuemax;
var currentStep = 0;

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	if (currentStep = parseInt(url.split('#slide')[1])) {
		$('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]').tab('show') ;
		//console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
	} else {
		$('#submenu.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
	}
	
} 

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})



function updateProgressBar(e, step) {
	if (!step) {
		currentStep = parseInt($(e.target).data('step'));
	} else {
		currentStep = step;
	}
	
	   //console.log("e: "+currentStep);
		if (!parseInt(currentStep)) {
		   currentStep = 1;
	   }
	  // console.log("e2: "+currentStep);

  
  var percent = (parseInt(currentStep) / maxNum) * 100;

  
  $('.progress-bar').css({width: percent + '%'});
  $('.lessonProgressText').text("Step " + currentStep + " of " + maxNum);
  
  if (currentStep === 1) {
	  $(".pagination .next").html('Start').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").addClass('disabled');
	 // $(".pagination .prev").click(function(e) {
	//	e.preventDefault();
	 // });
  }
  else if (currentStep === parseInt(maxNum)) {

	  $(".pagination .next").addClass('disabled');
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
	 // $(".pagination .next").click(function(e) {
	//	e.preventDefault();
	 // });	  
  }
  else {
	  $(".pagination .next").html('Next Slide →').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
  }
  //e.relatedTarget // previous tab

	validateCheckPoint();
}

$('a[data-toggle="tab"][role="tab-slides"]').on('shown.bs.tab', updateProgressBar );

function nextTab(elem) {
    var elemFind = $(elem).parent().next().find('a[data-toggle="tab"]')
	if (elemFind) {
		elemFind.click();
	}
}
function prevTab(elem) {
    var $elemFind = $(elem).parent().prev().find('a[data-toggle="tab"]')
	if ($elemFind) {
		$elemFind.click();
	}
}

$('.first').click(function(){

  $('#myWizard a:first').tab('show')

})


$(document).ready(function () {
	// console.log('echo1');
		updateProgressBar("lessonProgressBar", currentStep);
 //  console.log('echo2');


	//Initialize tooltips
    	$('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $("#lesson_tab .next").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        $active.parent().next().removeClass('disabled');
        nextTab($active);

    });
    $("#lesson_tab .prev").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        prevTab($active);

    });
	
	    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		html: true,
		trigger: 'manual'
}).click(showPopover, hidePopover).on('shown.bs.tooltip', function(){
	   $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
    });
	
	     // // swipe between lesson/editor/preview
var element = document.getElementById('mySwipe'),
    prevBtn = document.getElementById('backToLesson');

     
      window.mySwipe = new Swipe(element, {
        startSlide: 0,
        draggable: true,
        autoRestart: false,
        continuous: true,
        disableScroll: false,
        stopPropagation: true,
        callback: function(index, element) {
      	if (mySwipe.getPos() != 1) {
      		$('.CodeMirror-code').blur();
      		//console.log(mySwipe.getPos());		
      	} else  {
      		$('.CodeMirror-code').focus();
      		//console.log(mySwipe.getPos());
					
      	}
        },
        transitionEnd: function(index, element) {}
      });
prevBtn.onclick = mySwipe.prev;



});





