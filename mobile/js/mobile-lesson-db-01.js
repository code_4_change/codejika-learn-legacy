var lesson_id = 1;
var auth_id = "my7n4sM81zdnhxHzirawc4wOX9M2";


var delay;
var lesson_data = null;
var lesson_progress = null;
var slide_data = null;
var user_skills = null;
var active_slide;
var active_tab;
var save_pending = false;
var checkpoint_id = 0;
var checkpoint_count = 0;
var checkpoint_completed = 0;
var progressPercentage;
var current_avatar = "robot";
var swiperTabs;
var swiperLesson;

var DEBUG = true;


if (!DEBUG) {
  if (!window.console) window.console = {};
  var methods = ["log", "debug", "warn", "info"];
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () {};
  }
}

function depreciated_initFirebase(callback) {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  }

  firebase.initializeApp(config);


  firebase.database().ref('/lessons/' + lesson_id).once('value').then(function (snapshot) {
    lesson_data = snapshot.val();

    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 
  });

  firebase.database().ref('/user_profile/' + auth_id + '/profile').once('value').then(function (snapshot) {
    user_profile = snapshot.val();
    //	str = JSON.stringify(user_profile, null, 4);
    //console.log("user_profile:\n"+str); 

  });

  firebase.database().ref('/user_profile/' + auth_id + '/skills').once('value').then(function (snapshot) {
    user_skills = snapshot.val();
    //	str = JSON.stringify(user_skills, null, 4);
    //console.log("user_skills:\n"+str); 

  });

  firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id).once('value').then(function (snapshot) {
    lesson_progress = snapshot.val();

    //if no lesson data save default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found");
      lesson_progress = {
        "last_checkpoint": "0",
        "user_code": lesson_data.defaultCode,
        "progress_completed": "0"
      }
      saveUserData(true);
    }
    str = JSON.stringify(lesson_progress, null, 4);
    console.log("default lesson_progress inserted:\n" + str);

  });
  callback();
}

function timeNow() {
  return new Date().toISOString();
}

function saveUserData(save_code) {
  var now = new Date().toISOString();
  if (save_pending || save_code) {
    console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
    firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id).update({
      user_code: editor.getValue(),
      last_checkpoint: checkpoint_id,
      progress_completed: lesson_progress.progress_completed,
      last_updated: timeNow()
    });
    console.log("Saved code and checkpoint #" + checkpoint_id + " " + timeNow());
    save_pending = false;
  } else {
    //console.log("Nothing to save: "+timeNow());
  }
}

function resetUserData() {
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id).remove();

}

function resetCurrentProject() {
  resetUserData();
  localStorage.setItem("active_slide", 0);
  window.location.reload();
}

function checkpointCompleted(completed_id) {
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id + '/user_checkpoint/' + completed_id).update({
    completed: true,
    completed_at: timeNow()
  });
  checkpoint_completed++;

  if (checkpoint_completed === checkpoint_count) {
    lesson_progress.progress_completed = "100";
    lesson_status = "completed";
    console.log("checkpointCompleted: " + completed_id + " " + timeNow() + " " + checkpoint_completed + " " + checkpoint_count);
  } else {
    lesson_progress.progress_completed = Math.round((checkpoint_id * 100) / (checkpoint_count + 1));
    console.log(checkpoint_id + " * 100 / " + checkpoint_count + " = " + lesson_progress.progress_completed);
  }

  //saveUserData();
}

function initCodeEditor(custom_layout) {
  // Initialize CodeMirror editor
  editor = CodeMirror.fromTextArea(document.getElementById("code-editor"), {
      mode: "htmlmixed",
      tabMode: "indent",
      theme: 'base16-dark',
      styleActiveLine: true,
      lineNumbers: true,
      lineWrapping: true,
      autoCloseTags: false,
      foldGutter: false,
      dragDrop: true,

      lint: true,
      onKeyEvent: function (e, s) {
        if (s.type == "keyup") {
          CodeMirror.showHint(e);
        }
      },
      gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
    }),
    inf = editor.getInputField();

  $.keyboard.keyaction.undo = function (base) {
    base.execCommand('undo');
    return false;
  };
  $.keyboard.keyaction.redo = function (base) {
    base.execCommand('redo');
    return false;
  };

  $(inf).keyboard({
    keyBinding: "pointerdown",
    usePreview: false,
    useCombos: false,
    autoAccept: true,
    layout: 'custom',
    customLayout: {
      'normal': [
        custom_layout
      ]
    },
    position: false,
    beforeInsert: function (evnt, keyboard, elem, txt) {
      var position = editor.getCursor();
      if (txt === "\b") {
        editor.execCommand("delCharBefore");
      }
      if (txt === "\b" && position.ch === 0 && position.line !== 0) {
        elem.value = editor.getLine(position.line) || "";
        txt = "";
      }
      return txt;
    }
  });
}

function updatePreview() {
  console.log('update preview');
  var previewFrame = document.getElementById('preview');
  var preview = previewFrame.contentDocument || previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
  save_pending = true;
  validateCheckpoint();
}

function successAnimation() {
	//if (active_tab > 0) {
		$(".success-animation").removeClass('hide');

		setTimeout(function () {
			$(".success-animation").addClass('hide');
			console.log('animate2');
		}, 2500);
	//}
		console.log('animate1'+active_tab);
}

function buildSlides() {
  var slide_checkpoint_id = 1;
  var slide_indexes = [];
  var unlocked_class = "";

  slide_data = lesson_data.slides[active_slide];
  all_slides = lesson_data.slides;

  str = JSON.stringify(all_slides, null, 4);
 // console.log("slide_data:\n" + str);

  $.each(all_slides, function (index) {
    //console.log(all_slides[index]);

    if (!!all_slides[index].css_class) {
      custom_class = all_slides[index].css_class;
    } else {
      custom_class = "";
    }
    //		console.log("checkpoint: "+index); 
    if (all_slides[index].checkpoint === true) {
      //cp_class = " checkpoint";

      if (lesson_progress.user_checkpoint !== undefined && lesson_progress.user_checkpoint[slide_checkpoint_id] !== undefined) {
        unlocked_class = " cp-unlocked";
        //console.log("checkpoint completed: "+slide_checkpoint_id); 
        checkpoint_completed++
      } else {
        console.log("checkpoint not completed " + slide_checkpoint_id);
      }
      checkpoint_count++;
      slide_indexes.push(index);
    } else {
      //cp_class = "";
      unlocked_class = "";

    }

    $("#lesson_page .swiper-wrapper").append("<div class=\"swiper-slide " + custom_class + unlocked_class + " \" id=\"slide" + (index + 1) + "\">\n" + all_slides[index].html_content + "\n</div>");
    if (all_slides[index].checkpoint === true) {
      $('#lesson_page .swiper-wrapper .swiper-slide:nth-child(' + (index + 1) + ')').attr('data-checkpoint_id', slide_checkpoint_id++);
      //console.log("cp_index: "+slide_checkpoint_id);
    }

  });

  if (localStorage.getItem('active_slide') !== null) {
    active_slide = localStorage.getItem('active_slide');
    console.log(active_slide + "not null");
  } else {
    if (lesson_progress.last_checkpoint > 0) {
      active_slide = slide_indexes[lesson_progress.last_checkpoint - 1] + 1;
      console.log(lesson_progress.last_checkpoint + " null__ " + active_slide);
    } else {
      active_slide = 0;
      //console.log(lesson_progress.last_checkpoint+" null__3 "+active_slide);
    }
    //active_slide = lesson_progress.last_checkpoint;
    localStorage.setItem("active_slide", active_slide);

    //str = JSON.stringify(slide_indexes, null, 4);
    //console.log("slide_indexes:\n"+str); 
  }

  //console.log("checkpoint_count: "+checkpoint_count); 
  //$( "#lesson_page .swiper-wrapper" )
  //$('#lesson_page .swiper-wrapper .swiper-slide:nth-child(10)').addClass('active');
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index]);
    });
  }

}

function unlockSkills(skills) {
  //console.log("skills: "+skills);
  $("#" + skills + "-skill").addClass('has-skill-true');
  $("#" + skills + "-skill").removeClass('has-skill-false');
  //console.log('Unlocked skill '+skills);
  //console.log("#"+slide_id+"-skill");

  $("#" + skills + "-skill").attr('data-original-title', $("#" + skills + "-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
}

function test123() {
  console.log("test123");
}

function validateCheckpoint() {
  //slide_data = lesson_data.slides[active_slide-1];
  //active_slide = swiperV.activeIndex+1;
  //console.log('Trigger validation'+slide_data.regex);
  str = JSON.stringify(slide_data, null, 4);
  //console.log("slide_data:\n" + str);

  slide_id = active_slide + 1;
  if (slide_data.checkpoint === true) {
    if (!$("#slide" + slide_id).hasClass('cp-unlocked')) {

    }

  }
  if (slide_data.action === true) {
    console.log('action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
    if (slide_data.regex !== undefined) {
      console.log('validate_code trigger: ' + slide_data.regex);
      // do something
      console.log('Run validation on ' + active_slide);
      var re = slide_data.regex;
      var reResults = new RegExp(re, "i").test(editor.getValue());
      console.log('Run validation results ' + reResults);
      //	$(".check-info").removeClass('d-none');			
      if (reResults) {
        $("#slide" + slide_id + " .check-icon").html('/');
        $("#slide" + slide_id + " .actions li").addClass('correct');
        $(".check-info").addClass('correct');
        checkpoint_id = $("#slide" + slide_id).data("checkpoint_id");

        if (!$("#slide" + slide_id).hasClass('cp-unlocked')) {
          $("#slide" + slide_id).addClass('cp-unlocked');
          checkpointCompleted(checkpoint_id);
        } //else { //else used so don't save data twice
        saveUserData(true);
        //}
        successAnimation();

        $(".check-info .check-icon").html('/');
        $(".pagination .next").addClass('disabled');
        //$(".pagination .next").removeClass('disabled');
        console.log('Run validation using: ' + re);
        //$(".success-animation").show();	

        //console.log('checkpoint_id: '+checkpoint_id);


        if (slide_data.unlock_skills !== undefined) {
          unlockSkills(slide_data.unlock_skills);

          if (user_skills !== null) {
            user_skills.push(slide_data.unlock_skills);
            var unique_user_skills = [];
            $.each(user_skills, function (i, el) {
              if ($.inArray(el, unique_user_skills) === -1) unique_user_skills.push(el);
            });
            console.log("set unique unlockSkills: " + unique_user_skills);
            firebase.database().ref('/user_profile/' + auth_id + '/skills').set(unique_user_skills);

          } else {
            user_skills = {
              "0": slide_data.unlock_skills
            };
            console.log("set unlockSkills: " + user_skills);
            firebase.database().ref('/user_profile/' + auth_id + '/skills').set(user_skills);
          }


        }

      } else {
        $("#slide" + slide_id + " .check-icon").html('\\');
        $("#slide" + slide_id + " .actions li").removeClass('correct');
        $(".check-info .check-icon").html('\\');
        $(".check-info").removeClass('correct');
        $(".pagination .next").addClass('disabled');
      }
    }
    if (slide_data.js_function !== null) {
      console.log('js_function trigger: ' + slide_data.js_function);
      //var func = new Function("console.log('i am a new function')");
      var func = new Function(slide_data.js_function);
      func();
    }
  } else {
    console.log('no action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
  }
}

function progressUpdate() {
  var progressPercentage = checkpoint_count / checkpoint_id;
  firebase.database().ref('user_profile/' + auth_id + '/lesson_progress/' + lesson_id).set({
    progressCompleted: progressPercentage
  });
}

function inactivityTime() {
  var t;
  window.onload = resetTimer;
  document.onload = resetTimer;
  document.onmousemove = resetTimer;
  document.onmousedown = resetTimer; // touchscreen presses
  document.ontouchstart = resetTimer;
  document.onclick = resetTimer; // touchpad clicks
  document.onscroll = resetTimer; // scrolling with arrow keys
  document.onkeypress = resetTimer;

  //console.log("inactivityTime");
  resetTimer();

  function timedOut() {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
      attachAfter: '.modalPlaceholder',
      title: 'Are you still there?',
      accept: 'Yes',
      refuse: 'Yes, let\'s continue',
			popupStyle: 'speech-bubble '+current_avatar+' unsure', 
      refuseStyle: 'light action',
      acceptStyle: 'hide'
        //acceptAction: closeModal('#timed-out')
    });
  }

  function resetTimer() {
    if ($("#timed-out").length == 0) {
      //console.log("reset timer");
      clearTimeout(t);
      t = setTimeout(timedOut, 60000)
    }
  }
}

function closeModal(options) {
  console.log("close modal: " + options);
  $(options).modal('hide');
}

function onStart() {

  //TO DO: below not working to set kb layout using DB value
  //var customKB = lesson_data.kbLayout; 
  //initCodeEditor(customKB);

  initCodeEditor("< > /  =");
  editor.setValue(lesson_progress.user_code);

  buildSlides();
  console.log("start" + active_slide);
  saveUserData(true);
  //Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);

    if ($target.parent().hasClass('disabled')) {
      return false;
    }
  });

  //  Activate the Tooltips
  $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
    html: true,
    trigger: 'manual'
  }).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function () {
    $('#' + $(this).attr('id') + '.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
  });
  loadSkills();


  setInterval(saveUserData, 10000);

  // Live preview
  editor.on('change', function () {
    clearTimeout(delay);
    //console.log('preview');
    delay = setTimeout(updatePreview, 300);
  });
  var lessonLoaded = false;

  //initFullPage();

  $("#fp-nav ul li:nth-child(2) a span").addClass('cp-green');
  $("#fp-nav ul li:nth-child(12) a span").addClass('cp-red');

  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowToXuchMove: false,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true,
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () {

      },
      slideChange: function () {
        var slider = this;
				active_tab = slider.activeIndex;
        $('#lesson-navbar li.active').removeClass('active');
        var mindex = slider.activeIndex + 1;

        $('#lesson-navbar li:nth-child(' + mindex + ')').addClass('active');
        //console.log('#lesson-navbar:nth-child('+mindex+')');
        if (slider.activeIndex === 1) {
          $('.CodeMirror-code').focus();
          editor.focus();
          $('.ui-keyboard').show();
        } else {
          $('.CodeMirror-code').blur();
          $('.ui-keyboard').hide();

        }
        if (slider.activeIndex <= 0) {
          $(".success-animation").addClass('hide');
        }
        if (slider.activeIndex <= 2) {
          slider.allowSlideNext = true;
        } else {
          slider.allowSlideNext = false;
        }
          console.log("tab: "+active_tab);			  
      },
    }

  });
  swiperV = new Swiper('.swiper-container-v', {
    direction: 'vertical',
    spaceBetween: 50,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    preloadImages: false,
    //initialSlide: active_slide,
    // Enable lazy loading
    lazy: true,
    pagination: {
      el: '.swiper-pagination-v',
      clickable: false,
    },
    observer: true,
    observeParents: true,

    on: {
      init: function () {

      },
      slideChange: function () {
        var slider = this;
        //var mindex = slider.activeIndex+1;
        active_slide = slider.activeIndex;
        localStorage.setItem("active_slide", active_slide);
        slide_data = lesson_data.slides[active_slide];
        var unlocked = $("#slide" + (active_slide + 1)).hasClass("cp-unlocked");
        if (!unlocked && slide_data.action !== undefined) {
          slider.allowSlideNext = false;
          console.log('lock' + slider.allowSlideNext + active_slide);
        } else {
          slider.allowSlideNext = true;
          console.log('unlock' + active_slide);
        }
        //console.log("mindex: "+mindex);
        console.log("active_slide: " + active_slide);

        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').prevAll().addClass('bullet-green');
        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')').next().removeClass('bullet-green');
        validateCheckpoint();
      },
    }
  });

  swiperTabs = document.querySelector('.swiper-container-h').swiper;
  swiperLesson = document.querySelector('.swiper-container-v').swiper;

  $('#lesson-navbar li').click(function () {
    if ($(this).index() === 4) {
      console.log(swiperH.activeIndex);
      // console.log($(this).index());
      swiperH.allowSlideNext = true;
      swiperH.slideTo($(this).index());
      swiperH.allowSlideNext = false;

    } else {
      //console.log("dfd"+swiperH.activeIndex);
      // console.log("dfd"+$(this).index());
      swiperH.slideTo($(this).index());
      $('#lesson-navbar li.active').removeClass('active');
      $(this).addClass('active')

    }
  })
  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1);
    $('.swiper-pagination-switch').removeClass('active');
    $(this).addClass('active')
  })
  $('.swiper-next').click(function () {
    swiperLesson.allowSlideNext = true;
    swiperLesson.slideNext();
    //swiperLesson.allowSlideNext = false;
    console.log("skip");
  })
  $('.swiper-editor').click(function () {
    swiperTabs.allowSlideNext = true;
    swiperTabs.slideNext();
    //swiperTabs.allowSlideNext = false;
    console.log("skip2");
  })
  swiperLesson.slideTo(active_slide);
  //active_slide = 0;

  $('.reset-profile').click(function () {
    resetCurrentProject();
  });


  $("#profile_page .profile_name").append(", " + user_profile.username);

  $('.reset-lesson').click(function () {
    $.createDialog({
      modalName: 'reset-lesson',
      attachAfter: '.modalPlaceholder',
      title: 'Are you sure you want to reset this lesson?',
      accept: 'Yes',
      refuse: 'Hmmm, maybe not',
			popupStyle: 'speech-bubble '+current_avatar+' scared', 
      refuseStyle: 'light cancel',
      acceptStyle: 'light action reset-profile',
      acceptAction: resetCurrentProject
    });
  });

  inactivityTime();



}

function changeAvatar(avatar) {
  
	$(".modal-content").removeClass(current_avatar).addClass(avatar);
	$(".select_robot, .select_lego").toggleClass("avatar_selected avatar_unselected");
	//console.log("changed avatar css "+current_avatar+" with "+avatar);
	current_avatar = avatar;
	successAnimation();
}


function initFirebase() {
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  }

  firebase.initializeApp(config);
}

function getLessonDataPromise() {

  return firebase.database().ref('/lessons/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonDataPromise");
}

function getUserProfilePromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/profile').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserProfilePromise");
}

function getUserSkillsPromise() {

  return firebase.database().ref('/user_profile/' + auth_id + '/skills').once('value').then(function (snapshot) {
    return snapshot.val();

  }, function (error) {
    console.log(error);
  });
  console.log("getUserSkillsPromise");
}

function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_id).once('value').then(function (snapshot) {
    return snapshot.val();
  }, function (error) {
    console.log(error);
  });
  console.log("getLessonProgressPromise");
}



$(document).ready(function () {

  initFirebase();

  var getLessonData = getLessonDataPromise();
  var getUserProfile = getUserProfilePromise();
  var getUserSkills = getUserSkillsPromise();
  var getLessonProgress = getLessonProgressPromise();

  Promise.all([getLessonData, getUserProfile, getUserSkills, getLessonProgress]).then(function (results) {
    lesson_data = results[0];
    //str = JSON.stringify(lesson_data, null, 4);
    //console.log("lesson_data:\n"+str); 

    user_profile = results[1];
    //str = JSON.stringify(user_profile, null, 4);
    //console.log("user_profile:\n"+str); 

    user_skills = results[2];
    //str = JSON.stringify(user_skills, null, 4);
    //console.log("user_skills:\n"+str); 

    lesson_progress = results[3];

    //if no lesson data save default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found");
      lesson_progress = {
          "last_checkpoint": "0",
          "user_code": lesson_data.defaultCode,
          "progress_completed": "0"
        }
        //saveUserData(true);
      console.log("default lesson_progress inserted");
    }

    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str); 

    console.log("promise all confirm and running onStart");
    onStart();
  });

	$(".select_lego").click(function(){changeAvatar("lego")});
	$(".select_robot").click(function(){changeAvatar("robot")});

});