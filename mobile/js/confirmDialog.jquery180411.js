
 (function( $ ){
	$.createDialog = function(options){
					$('.modal , .modal-backdrop').remove(); 
			console.log("clicked "+options.modalName);
		$('.modalPlaceholder').after(
			"<div class=\"modal fade\" id=\""+options.modalName+"\">\n"+
			"<div class=\"modal-dialog\">\n"+
				"<div class=\"modal-content "+options.popupStyle+"\">\n"+
				"<div class=\"modal-body\">\n"+
				options.htmlContent+
				"</div>\n"+
				"</div>\n"+
			"</div>\n"+
			"</div>\n"
		);
		//$('#confirm_title').html(options.title);
		//$('#confirm_no').html(options.refuse);
		//$('#confirm_yes').html(options.accept);
		
		//$(options.modalName+' .modal-content').addClass(options.popupStyle);
		//$('#confirm_no').addClass(options.refuseStyle);
		//$('#confirm_yes').addClass(options.acceptStyle);
		
		$('#action_button').bind("click", options.actionButton);
		$('#close_button').bind("click", function (e) { 
			$('#'+options.modalName).modal('hide'); 
			$('.modal , .modal-backdrop').remove(); 
		});
		$('#'+options.modalName).modal('show');
		/*$('#'+options.modalName).on('hidden.bs.modal', function (e) {
			console.log("on hidden");
			//$('#'+options.modalName).remove();
			$('#'+options.modalName).modal('dispose');
			//		$('#'+options.modalName).remove();
			inactivityTime();
		}); */
	}
})( jQuery );