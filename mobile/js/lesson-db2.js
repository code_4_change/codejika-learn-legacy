var delay;
var lesson_data = null;
var currentStep;
	str = JSON.stringify(lesson_data, null, 4);
	console.log("user_data1:\n"+str); 
	
 // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  };
  
  firebase.initializeApp(config);


firebase.database().ref('/lessons/1').once('value').then(function(snapshot) {
  lesson_data = snapshot.val();


str = JSON.stringify(lesson_data, null, 4);
console.log("lesson_data:\n"+str); 
});	

firebase.database().ref('/user_profile/my7n4sM81zdnhxHzirawc4wOX9M2/profile').once('value').then(function(snapshot) {
  user_data = snapshot.val();
  	str = JSON.stringify(lesson_data, null, 4);
	console.log("user_data:\n"+str); 
  
});


firebase.database().ref('/user_profile/my7n4sM81zdnhxHzirawc4wOX9M2/lesson_progress/1').once('value').then(function(snapshot) {
  lesson_progress = snapshot.val();

	editor.setValue(lesson_progress.code);
	str = JSON.stringify(lesson_data, null, 4);
	console.log("user_data:\n"+str); 
});

if (localStorage.getItem('currentStep') !== null) {
	currentStep = localStorage.getItem('currentStep');
	console.log(currentStep+"not null");
} else {
	currentStep = 1;
	localStorage.setItem("currentStep", currentStep);
		console.log(currentStep+" null");
}

function initCodeEditor() { 
	// CodeMirror HTMLHint Integration
	(function(mod) {
	  if (typeof exports == "object" && typeof module == "object") // CommonJS
		mod(require("../../lib/codemirror"));
	  else if (typeof define == "function" && define.amd) // AMD
		define(["../../lib/codemirror"], mod);
	  else // Plain browser env
		mod(CodeMirror);
	})

	(function(CodeMirror) {
	  "use strict";

	  CodeMirror.registerHelper("lint", "html", function(text) {
		var found = [], message;
		if (!window.HTMLHint) return found;
		var messages = HTMLHint.verify(text, ruleSets);
		for ( var i = 0; i < messages.length; i++) {
		  message = messages[i];
		  var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
		  found.push({
			from: CodeMirror.Pos(startLine, startCol),
			to: CodeMirror.Pos(endLine, endCol),
			message: message.message,
			severity : message.type
		  });
		}
		return found;
	  }); 
	});

	// ruleSets for HTMLLint
	ruleSets = {
	  "tagname-lowercase": true,
	  "attr-lowercase": true,
	  "attr-value-double-quotes": true,
	  "doctype-first": false,
	  "tag-pair": true,
	  "spec-char-escape": true,
	  "id-unique": true,
	  "src-not-empty": true,
	  "attr-no-duplication": true
	};



	// Initialize CodeMirror editor
	editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
	  mode: "htmlmixed",
	  tabMode: "indent",
	  theme: 'base16-dark',  
	  styleActiveLine: true,
	  lineNumbers: true,
	  lineWrapping: true,
	  autoCloseTags: false,
	  foldGutter: true,
	  dragDrop : true,
	  lint: true,
	  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
	});

}

function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
	
  validateCheckPoint();

}


function validateCheckPoint() {
str = JSON.stringify(lesson_data, null, 4);
console.log("lesson_data:\n"+currentStep); 
	slide_data = lesson_data.slides[currentStep];																				
	if( slide_data.checkpoint == true ) {
    // do something
		console.log('Run validation only on '+currentStep);
		var re = slide_data.regex;
		var reResults = new RegExp(re, "i").test(editor.getValue());
	 		console.log('Run validation results '+reResults);
		if (reResults) {
			$("#slide"+currentStep+" .check-icon").html('/');
			$("#slide"+currentStep+" .check").html('I did it →').addClass('passed');
			$("#slide"+currentStep+" .actions li").addClass('correct');
						$(".pagination .next").addClass('disabled');
			//$(".pagination .next").removeClass('disabled');
					console.log('Run validation using '+currentStep);
		} else {
			$("#slide"+currentStep+" .check-icon").html('\\');
			$("#slide"+currentStep+" .actions li").removeClass('correct');
			$(".pagination .next").addClass('disabled');
		}
	}
	

	if( slide_data.unlock_skills == true ) {
				$("#"+slide_data.unlock_skills+"-skill").addClass('has-skill-true');
				$("#"+slide_data.unlock_skills+"-skill").removeClass('has-skill-false');
		console.log('Unlocked skill '+slide_data.unlock_skills);
		console.log("#"+currentStep+"-skill");

	  $("#"+slide_data.unlock_skills+"-skill").attr('data-original-title', $("#"+slide_data.unlock_skills+"-skill").attr('data-title-text')).tooltip('show').tooltip('hide');
	 //$("#"+slide_data.unlock_skills+"-skill").attr('data-original-title',"passwords did not match").tooltip('show').tooltip('hide');
	  	  console.log("#"+slide_data.unlock_skills+"-skill");
	  console.log($("#"+slide_data.unlock_skills+"-skill").attr('data-title-text'));
	}	
}

//var activeTab = $(".tab-content .tab-content").find(".active");
//var id = activeTab.attr('id');
//console.log('Run validation only on '+currentStep);


function changeSlide() {
	
	 console.log('echo1a');
	 		updateProgressBar("lessonProgressBar", currentStep);
    console.log('echo2a');	
	localStorage.setItem("currentStep", currentStep);
}

	// DESKTOP ONLY
var lessonProgressBar = document.getElementById('lessonProgressBar');
var maxNum = lessonProgressBar.dataset.valuemax;

/*
// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	if (currentStep = parseInt(url.split('#slide')[1])) {
		$('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]').tab('show') ;
		console.log('#tab-slides.nav-tabs a[href="#slide'+currentStep+'"]');
	} else {
		$('#submenu.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
	}
	
} 
	*/
// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
})

function updateProgressBar(e, step) {
	if (!step) {
		currentStep = parseInt($(e.target).data('step'));
	} else {
		currentStep = step;
	}
	
	   console.log("e: "+currentStep);
		if (!parseInt(currentStep)) {
		   currentStep = 1;
	   }
	   console.log("e2: "+currentStep);

  
  var percent = (parseInt(currentStep) / maxNum) * 100;

  
  $('.progress-bar').css({width: percent + '%'});
  $('.lessonProgressText').text("Step " + currentStep + " of " + maxNum);
  
  if (currentStep === 1) {
	  $(".pagination .next").html('Start Slideshow').removeClass('disabled');
	  $(".pagination .prev").addClass('disabled');
	 // $(".pagination .prev").click(function(e) {
	//	e.preventDefault();
	 // });
  }
  else if (currentStep === parseInt(maxNum)) {

	  $(".pagination .next").addClass('disabled');
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
	 // $(".pagination .next").click(function(e) {
	//	e.preventDefault();
	 // });	  
  }
  else {
	  $(".pagination .next").html('Next slide <i class="fa next-icon fa-step-forward">').removeClass('disabled').attr("href", "#slide"+(currentStep+1));
	  $(".pagination .prev").removeClass('disabled').attr("href", "#slide"+(currentStep-1));
  }
  //e.relatedTarget // previous tab

	validateCheckPoint();
}

function nextTab(elem) {
    var elemFind = $(elem).parent().next().find('a[data-toggle="tab"]')
	if (elemFind) {
		elemFind.click();
	}
}

function prevTab(elem) {
    var $elemFind = $(elem).parent().prev().find('a[data-toggle="tab"]')
	if ($elemFind) {
		$elemFind.click();
	}
}



$(document).ready(function () {
	initCodeEditor();
	// DESKTOP AND MOBILE
	
	
		//Initialize tooltips
    	$('.nav-tabs > li a[title]').tooltip();
		
	    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip({
		html: true,
		trigger: 'manual'
}).click(showPopover).hover(showPopover, hidePopover).on('shown.bs.tooltip', function(){
	   $('#'+$(this).attr('id')+'.has-skill-true .skills-example').clone().appendTo('.skill-details-content');
    });
	
// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});	
//setTimeout(updatePreview, 300);


	// DESKTOP ONLY
	
$('.first').click(function(){

  $('#myWizard a:first').tab('show')

})

$('a[data-toggle="tab"][role="tab-slides"]').on('shown.bs.tab', changeSlide );



    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $("#lesson_tab .next").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        $active.parent().next().removeClass('disabled');
        nextTab($active);
		

    });
    $("#lesson_tab .prev").click(function (e) {

        var $active = $('#lesson_tab .nav-tabs li>a.active');
        prevTab($active);

    });
	
	
	
	
	
	


});


