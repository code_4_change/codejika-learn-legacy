// CodeMirror HTMLHint Integration
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [], message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for ( var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line -1, endLine = message.line -1, startCol = message.col -1, endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity : message.type
      });
    }
    return found;
  }); 
});

// ruleSets for HTMLLint
var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

var delay;

// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',  
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});

var editor = CodeMirror.fromTextArea($('#keyboard')[0], {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'base16-dark',  
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop : true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
    	}),
        input = editor.getInputField();

    $.extend($.keyboard.keyaction, {
        delete: function (base) {
            editor.execCommand('delCharBefore');
        },
        left: function (base) {
            editor.execCommand('goCharLeft');
        },
        right: function (base) {
            editor.execCommand('goCharRight');
        }
    });

    $(input).keyboard({
        keyBinding: "mousedown touchstart",
        usePreview: false,
        autoAccept: true,
        alwaysOpen: true,
        position: {
            of: $("#wrap"),
            my: 'center bottom',
            at: 'center bottom',
            at2: 'center bottom'
        },
        layout: 'custom',
        customLayout: {
            'normal': [
                '! @ # <  > ( ) / \ . {bksp} {left} {right}']
        }
    });
})




//editor.setValue(localStorage.getItem("code"));
$("#lesson1").click(function(e){

        var value = editor.getValue();
            if(value.length == 0) {
                alert("Missing comment!");
            }
		if (typeof(Storage) !== "undefined") {
            // Store
            localStorage.setItem("code", value);
            // // Retrieve
            // //document.getElementById("result").innerHTML = localStorage.getItem("lastname");
            // alert(localStorage.getItem("code"));
            window.location.href = 'learn-html-css-javascript-02.htm';
        } else {
            //document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
            alert("Sorry, your browser does not support Web Storage")
        }
	});
// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});
function updatePreview() {
  var previewFrame = document.getElementById('preview');
  var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
  preview.open();
  preview.write(editor.getValue());
  preview.close();
	

}



setTimeout(updatePreview, 300);




