<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
        var start = Date.now();
    </script>
    <meta charset="utf-8">
    <title>Code JIKA - Learn HTML, CSS and Javascript</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <script src='/js/jquery-1.11.0.min.js' type='text/javascript' id="js-jquery" async></script>
    <script src="/js/firebase.js" id="js-firebase" async></script>
    <script type='text/javascript' src='/js/mobile-lesson-db.js' async></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="/css/keyboard.css"/>
    <link href='/css/codemirror.css' rel='stylesheet' type="text/css"/>
    <link href="/codemirror/addon/hint/show-hint.css" rel="stylesheet" type="text/css"/>
    <link href="/codemirror/addon/lint/lint.css" rel="stylesheet" type="text/css"/>
    <link href='/css/theme/base16-dark.css' rel='stylesheet' type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel='stylesheet' type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:500,700" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">

        function randText() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }

        document.write("<link rel='stylesheet' type='text/css' href='/css/mobile-lesson-db.css?" + randText() + "'\/>");
    </script>
    <style>

        <
        style >
        .swiper-pagination-bullet {
            background: #444;
            opacity: .9;
        }

        .swiper-container-vertical > .swiper-pagination-bullets .swiper-pagination-bullet {
            margin: 5px 0 5px 2px;
        }

        .swiper-pagination-bullet {
            width: 5px;
            height: 5px;
            display: inline-block;
            border-radius: 100%;
            background: #000;
            opacity: .2;
        }

        .swiper-pagination-bullet {
            background: #444;
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            opacity: 1 !important;
            background: #fff !important;
            border: 1px solid #444;
            width: 9px;
            height: 9px;
            margin: 5px 0 5px 0 !important;
        }

    </style>
    </

    style

    >
    <
    /
    head >
    < body >
    < div id

    =
    "header"
    class

    =
    "embed-nav"
    >
    < a href

    =
    "https://
    <?php echo $_SERVER['SERVER_NAME']?>
    "
    style

    =
    "padding: 0!important;"
    > < img src

    =
    "/img/logo.png"
    alt

    =
    "logo"
    id

    =
    "logo"
    style

    =
    ""
    /
    > <

    /
    a >
    < ul id

    =
    "lesson-navbar"
    class

    =
    "hXide2"
    style

    =
    "position: absolute; right: 0; top: 0; border: 0;"
    >
    < li id

    =
    "menu-lesson"
    class

    =
    "active"
    > < a > < span > LESSON

    01
    <
    /
    span > < i class

    =
    "fas fa-list-alt"
    > <

    /
    i > <

    /
    a > <

    /
    li >
    < li id

    =
    "menu-code"
    > < a > < span > CODE<

    /
    span > < i class

    =
    "fas fa-code"
    > <

    /
    i > <

    /
    a > <

    /
    li >
    < li id

    =
    "menu-preview"
    > < a > < span > PREVIEW<

    /
    span > < i class

    =
    "fas fa-desktop"
    > <

    /
    i > <

    /
    a > <

    /
    li >
    < li id

    =
    "menu-skills"
    > < a > < span > SKILLS<

    /
    span > < i class

    =
    "fas fa-graduation-cap"
    > <

    /
    i > <

    /
    a > <

    /
    li >
    < li id

    =
    "menu-profile"
    > < a > < span > MENU<

    /
    span > < i class

    =
    "fas fa-user-circle"
    > <

    /
    i > <

    /
    a > <

    /
    li >
    <

    /
    ul >
    <

    /
    div >
    < div class

    =
    "swiper-container swiper-container-h"
    >
    < div class

    =
    "swiper-wrapper"
    >
    < div class

    =
    "swiper-slide"
    id

    =
    "lesson-page"
    >
    < div class

    =
    "swiper-container swiper-container-v"
    >
    < div class

    =
    "swiper-wrapper cm-s-base16-dark"
    >
    <

    /
    div >
    < div class

    =
    "swiper-pagination swiper-pagination-v"
    > <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "swiper-slide"
    id

    =
    "editor_page"
    >
    < textarea id

    =
    'code-editor'
    name

    =
    'editor'
    style

    =
    "display: none;"
    >
    <

    /
    textarea >
    < div id

    =
    "virt-keyboard"
    class

    =
    "hide"
    >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "swiper-slide"
    id

    =
    "preview_page"
    >
    < iframe class

    =
    ""
    id

    =
    'preview'
    name

    =
    "preview"
    > <

    /
    iframe >
    < div class

    =
    "swipe-overlay"
    > <

    /
    div >
    <

    /
    div >
    < div class

    =
    "swiper-slide"
    id

    =
    "skills_page"
    >
    < div class

    =
    "container"
    >
    < h2 style

    =
    "user-select: text;"
    > My coding skills<

    /
    h2 >
    < div class

    =
    'skills'
    >
    < span id

    =
    "h1-h6-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ''
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;h1-h6&lt;/h2&gt; &lt;p class="skill-description"&gt;Document headings and sub-headings. The bigger the number, the smaller the size.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > h1-h6<

    /
    span >
    < div class

    =
    "skills-example"
    >
    < span id

    =
    "h1-h6-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;h1-h6&lt;/h2&gt; &lt;p class="skill-description"&gt;Document headings and sub-headings. The bigger the number, the smaller the size.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "CodeTooltip"
    >
    < span id

    =
    "h1-h6-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;h1-h6&lt;/h2&gt; &lt;p class="skill-description"&gt;Document headings and sub-headings. The bigger the number, the smaller the size.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "code"
    >
    < span id

    =
    "h1-h6-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;h1-h6&lt;/h2&gt; &lt;p class="skill-description"&gt;Document headings and sub-headings. The bigger the number, the smaller the size.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > < code > < span class

    =
    "tag"
    > & lt

    ;
    h1& gt

    ;
    <
    /
    span > I& #8217

    ;
    m a main heading< span class

    =
    "tag"
    > & lt

    ;
    /
    h1& gt

    ;
    <
    /
    span > < br

    /
    >
    < span class

    =
    "tag"
    > & lt

    ;
    h2& gt

    ;
    <
    /
    span > I& #8217

    ;
    m a subheading< span class

    =
    "tag"
    > & lt

    ;
    /
    h2& gt

    ;
    <
    /
    span > < br

    /
    >
    < span class

    =
    "tag"
    > & lt

    ;
    h6& gt

    ;
    <
    /
    span > I& #8217

    ;
    m the smallest< span class

    =
    "tag"
    > & lt

    ;
    /
    h6& gt

    ;
    <
    /
    span > <

    /
    code >
    <

    /
    span >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div > < span id

    =
    "email-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;email input&lt;/h2&gt; &lt;p class="skill-description"&gt;A type of input that takes user email addresses. New in HTML5.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > email input<

    /
    span >
    < div class

    =
    "skills-example"
    >
    < span id

    =
    "email-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;email input&lt;/h2&gt; &lt;p class="skill-description"&gt;A type of input that takes user email addresses. New in HTML5.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "CodeTooltip"
    >
    < span id

    =
    "email-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;email input&lt;/h2&gt; &lt;p class="skill-description"&gt;A type of input that takes user email addresses. New in HTML5.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "code"
    >
    < span id

    =
    "email-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;email input&lt;/h2&gt; &lt;p class="skill-description"&gt;A type of input that takes user email addresses. New in HTML5.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < pre >
    < span id

    =
    "email-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;email input&lt;/h2&gt; &lt;p class="skill-description"&gt;A type of input that takes user email addresses. New in HTML5.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > < span class

    =
    "tag"
    > & lt

    ;
    input<

    /
    span > < span class

    =
    "attribute-name"
    > type<

    /
    span >

    =
    <
    span class

    =
    "string"
    > < span class

    =
    "delimiter"
    >

    "</span><span class="
    content

    ">email</span><span class=
    "delimiter"
    >

    "</span></span> <span class="
    attribute-name

    ">placeholder</span>=<span class=
    "string"
    > < span class

    =
    "delimiter"
    >

    "</span><span class=
    "content"
    > Your email<

    /
    span > < span class

    =
    "delimiter"
    >

    "</span></span><span class=
    "tag"
    > & gt

    ;
    <
    /
    span > <

    /
    span >
    <

    /
    pre >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div > < span id

    =
    "submit-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'submit-input'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;submit input&lt;/h2&gt; &lt;p class="skill-description"&gt;Makes a button to submit information from a form to a server.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > submit input<

    /
    span >
    < div class

    =
    "skills-example"
    >
    < span id

    =
    "submit-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'submit-input'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;submit input&lt;/h2&gt; &lt;p class="skill-description"&gt;Makes a button to submit information from a form to a server.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "CodeTooltip"
    >
    < span id

    =
    "submit-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'submit-input'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;submit input&lt;/h2&gt; &lt;p class="skill-description"&gt;Makes a button to submit information from a form to a server.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "code"
    >
    < span id

    =
    "submit-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'submit-input'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;submit input&lt;/h2&gt; &lt;p class="skill-description"&gt;Makes a button to submit information from a form to a server.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < pre >
    < span id

    =
    "submit-input-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    data-name

    =
    'submit-input'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;submit input&lt;/h2&gt; &lt;p class="skill-description"&gt;Makes a button to submit information from a form to a server.&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > < span class

    =
    "tag"
    > & lt

    ;
    input<

    /
    span > < span class

    =
    "attribute-name"
    > type<

    /
    span >

    =
    <
    span class

    =
    "string"
    > < span class

    =
    "delimiter"
    >

    "</span><span class="
    content

    ">submit</span><span class=
    "delimiter"
    >

    "</span></span><span class="
    tag

    ">&gt;</span></span>
    <
    /
    pre >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div > < span id

    =
    "p-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'hide()'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;P&lt;/h2&gt; &lt;p class="skill-description"&gt;The p tag makes paragraph elements&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > p<

    /
    span >
    < div class

    =
    "skills-example"
    >
    < div class

    =
    "CodeTooltip"
    >
    < span id

    =
    "p-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'hide()'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;P&lt;/h2&gt; &lt;p class="skill-description"&gt;The p tag makes paragraph elements&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < div class

    =
    "code"
    >
    < span id

    =
    "p-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'hide()'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;P&lt;/h2&gt; &lt;p class="skill-description"&gt;The p tag makes paragraph elements&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > <

    /
    span >
    < pre >
    < span id

    =
    "p-skill"
    class

    =
    'skill has-skill-false skill-type-html'
    'hide()'
    rel

    =
    'tooltip'
    title

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;Locked Skill&lt;/h2&gt; &lt;p class="skill-description"&gt;Your mind is not yet prepared to comprehend this skill&lt;/p&gt; &lt;p class="skill-description"&gt;Keep learning to unlock it :)&lt;/p&gt;'
    data-title-text

    =
    ' &lt;div class="skill-details"&gt; &lt;div class="code"&gt; &lt;div class="skill-details-content"&gt;&lt;h2 class="skill-name"&gt;P&lt;/h2&gt; &lt;p class="skill-description"&gt;The p tag makes paragraph elements&lt;/p&gt; &lt;h3&gt;Example&lt;/h3&gt; &lt;/div&gt; &lt;/div&gt;&lt;/div&gt;'
    data-placement

    =
    'bottom'
    target

    =
    '_blank'
    > < span class

    =
    "tag"
    > & lt

    ;
    p& gt

    ;
    <
    /
    span > I am a cool paragraph, full of text< span class

    =
    "tag"
    > & lt

    ;
    /
    p& gt

    ;
    <
    /
    span > <

    /
    span >
    <

    /
    pre >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "get-help"
    >
    < h3 > Stuck?

    <
    /
    h3 >
    < p style

    =
    "user-select: text;"
    > Ask the community for help in our Facebook group!

    <
    br

    /
    > < a href

    =
    "https://www.facebook.com/groups/626270557761252/"
    target

    =
    "_blank"
    class

    =
    "button"
    > Launch the Facebook Group<

    /
    a > <

    /
    p >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "swiper-slide"
    id

    =
    "profile_page"
    >
    < div class

    =
    "container"
    >
    < i class

    =
    "fas fa-user-circle fa-6x"
    style

    =
    "padding: 20px 0px 10px 0;"
    > <

    /
    i >
    < h2 class

    =
    "profile_name"
    > Hello<

    /
    h2 >
    < div class

    =
    "menu"
    >
    < ul >
    < li class

    =
    ""
    > < a class

    =
    "divLink"
    href

    =
    "https://
    <?php echo $_SERVER['SERVER_NAME']?>
    "
    > <

    /
    a > < i class

    =
    "fas fa-th-list fa-2x"
    > <

    /
    i > Homepage<

    /
    li >
    < li class

    =
    "reset-lesson"
    data-toggle

    =
    "modal"
    data-target

    =
    "#reset-lesson"
    > < i class

    =
    "fas fa-undo fa-2x"
    > <

    /
    i > Reset Current Lesson<

    /
    li >
    < li class

    =
    ""
    > < i class

    =
    "fas fa-smile fa-2x"
    > <

    /
    i > Instructor < div class

    =
    "option_selected select_robot"
    style

    =
    "margin-left: 5px;"
    > Robot<

    /
    div > < div class

    =
    "option_unselected select_lego"
    > Lego<

    /
    div > <

    /
    li >
    <

    /
    ul >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "swiper-pagination swiper-pagination-h"
    > <

    /
    div >
    <

    /
    div >
    <

    /
    div >
    < div class

    =
    "modalPlaceholder"
    > <

    /
    div >
    < div class

    =
    "success-animation hide"
    >

    /
    <
    /
    div >
    < script type

    =
    "text/javascript"
    src

    =
    "/js/jquery-ui.min.js"
    > <

    /
    script >
    < script src

    =
    "/js/pep.js"
    > <

    /
    script >
    < script src

    =
    '/js/popper.min.js'
    type

    =
    'text/javascript'
    > <

    /
    script >
    < script src

    =
    "/js/bootstrap.min.js"
    integrity

    =
    "sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin

    =
    "anonymous"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    '/js/now-ui-kit.js?v=1.1.0'
    type

    =
    'text/javascript'
    > <

    /
    script >
    < script src

    =
    "/js/codemirror-07961539f5608d3979748d198644081d30f758d024972d9faaf6120f651a6ebe.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/mode/javascript/javascript.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/mode/css/css.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/mode/htmlmixed/htmlmixed.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/htmlhint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/csslint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/jshint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/addon/lint/lint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/addon/lint/html-lint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/addon/lint/css-lint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/addon/lint/javascript-lint.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/codemirror/markdown.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/js/swiper.min.js"
    > <

    /
    script >
    < script type

    =
    "text/javascript"
    src

    =
    "/js/jquery.keyboard.js"
    > <

    /
    script >
    < script src

    =
    "/js/fa-solid.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/js/fa-regular.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script src

    =
    "/js/fontawesome.min.js"
    type

    =
    "text/javascript"
    > <

    /
    script >
    < script type

    =
    "text/javascript"
    src

    =
    "/js/jquery.keyboard.extension-typing.js"
    > <

    /
    script >
    < script type

    =
    "text/javascript"
    src

    =
    "/js/jquery.keyboard.extension-caret.js"
    > <

    /
    script >
    < script src

    =
    "/js/jquery.keyboard.extension-mobile.js"
    > <

    /
    script >
    < script src

    =
    "/js/confirmDialog.jquery.js"
    > <

    /
    script >
    < script src

    =
    "/js/jsplumb.min.js"
    > <

    /
    script >
    < script async src

    =
    "https://www.googletagmanager.com/gtag/js?id=UA-63106610-3"
    > <

    /
    script >
    < script >
    window.dataLayer

    =
    window.dataLayer | | []

    ;
    function

    gtag
    (
    )
    {
        dataLayer . push(arguments)
    }
    ;
    gtag
    (
    'js'
    ,
    new

    Date
    (
    )
    )
    ;

    gtag
    (
    'config'
    ,
    'UA-63106610-3'
    )
    ;
    <
    /
    script >
    <

    /
    body >
    <

    /
    html >