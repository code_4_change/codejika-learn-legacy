<!DOCTYPE html>
<html lang="en">

<head>
    <script type="text/javascript">
        var start = Date.now();
    </script>
    <meta charset="utf-8">
    <title>Code JIKA - Learn HTML, CSS and Jscript</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="/css/merged.min.css" rel="stylesheet" type="text/css"/>


    <script src="/js/jquery-1.3.2.min.js"></script>


    <script type="text/javascript">

        function randText() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }

        document.write("<link rel='stylesheet' type='text/css' href='/css/mobile-lesson-P0.css?" + randText() + "'\/>");
    </script>
    <style>
        .bottom_keys {
            bottom: 0;
            right: 0;
            left: 0;
            position: absolute;
            /*  need to set this - to retain overflow behavior:  */
            z-index: 9999;
            margin-right: 0;
            margin-left: 0;
        }

        .button_design {
            background-color: rgb(231, 236, 231);
            border: 0.5px solid black;
            color: rgb(20, 12, 12);
            padding: 5px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            border-radius: 2px;
            width: calc(100% / 4);
        }

        .swiper-pagination-bullet {
            background: #444;
            opacity: .9;
        }

        .swiper-container-vertical > .swiper-pagination-bullets .swiper-pagination-bullet {
            margin: 2px 0 2px 2px;
        }

        .swiper-pagination-bullet {
            width: 5px;
            height: 5px;
            display: inline-block;
            border-radius: 100%;
            background: #000;
            opacity: .2;
        }

        .swiper-pagination-bullet {
            background: #444;
            opacity: 1;
        }

        .swiper-pagination-bullet-active {
            opacity: 1 !important;
            background: #fff !important;
            border: 1px solid #444;
            width: 9px;
            height: 9px;
            margin: 2px 0 2px 0 !important;
        }

        #lesson-page .yellow {
            color: #ffc009 !important;
        }

        #lesson-page .pink {
            color: #f5e !important;
        }

        #lesson-page .blue {
            color: #03A9F4 !important;
        }

        #lesson-page .red {
            color: #ff6a00 !important;
        }

        .nobr {
            white-space: nowrap
        }

        .lesson-Xtitle {
            weight: normal;
        }

        #lesson-page h3.lesson-title {
            font-size: 32px;
            line-height: 32px;
        }

        #lesson-page p.lesson-instructions {
            font-size: 24px;
        }

        .w-15 {
            width: 15% !important;
        }

        .w-20 {
            width: 20% !important;
        }

        .op40 {
            opacity: .35
        }

        .op60 {
            opacity: .6
        }

        .lesson-tabs {
            max-width: 150px;
            text-align: center;
            font-size: 20px;
            line-height: 36px;
            font-family: 'Rajdhani', sans-serif;
            background-color: white;
            color: black;
            display: block;
            margin: 10px auto;
            text-transform: uppercase;

        }

        #lesson-page p.lesson-tip {
            padding: 6px;
            background-color: rgba(255, 255, 255, .2);
            margXin: 0 20px;
            font-size: 14px;
            position: absolute;
            bottom: 0;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            width: calc(100% - 40px);
        }

        #lesson-page p.lesson-tip span {
            color: #fd6f6f;
        }

        #lesson-page .slide-chapter-bg {
            background-color: #44e3e8 !important;
        }

        #lesson-page .checkpoint {
            background-color: #ffe712 !important;
        }

        #lesson-page .checkpoint p.lesson-tip {
            background-color: rgba(0, 0, 0, .1) !important;
        }

        #lesson-page .checkpoint .container {
            color: black;
        }

        #lesson-page .checkpoint p.lesson-tip span {
            color: #fc3535;
        }

        #lesson-page span.cm-bracket,
        #lesson-page span.cm-tag,
        #lesson-page span.cm-keyword,
        #lesson-page span.cm-error,
        #lesson-page span.cm-link,
        #lesson-page span.cm-string,
        #lesson-page span.cm-attribute,
        #lesson-page span.cm-property,
        #lesson-page span.cm-variable,
        #lesson-page span.cm-variable-2,
        #lesson-page .code-pre {
            font-size: 20px;
            line-height: 20px;
        }

        #lesson-page .code-pre span.cm-bracket,
        #lesson-page .code-pre span.cm-tag,
        #lesson-page .code-pre span.cm-keyword,
        #lesson-page .code-pre span.cm-error,
        #lesson-page .code-pre span.cm-link,
        #lesson-page .code-pre span.cm-string,
        #lesson-page .code-pre span.cm-attribute,
        #lesson-page .code-pre span.cm-property,
        #lesson-page .code-pre span.cm-variable,
        #lesson-page .code-pre span.cm-variable-2,
        #lesson-page .code-pre {
            font-size: 20px;
            line-height: 20px;

        }

        #lesson-page .checkpoint .code-pre span.cm-string {
            color: #fd9300;
        }

        #lesson-page .checkpoint span.cm-property,
        #lesson-page span.cm-attribute {
            color: #7ba420;
        }

        .checkpoint .actions li.correct .check-icon,
        .check-info.correct .check-icon {
            font-size:
        }

        .checkpoint .actions li.correct .check-icon,
        .check-info.correct .check-icon {
            background-color: initial;
            color: #00A921;
        }

        .btn-primary.success img {
            width: 40px;
        }

        #lesson-page .btn-primary,
        .btn-primary {
            border-bottom: initial;
            font-size: 20px;
        }

        #lesson-page .btn-primary.success {
            background-color: #68c300 !important;
        }

        .success-animation {
            background: none;
            top: calc(50% - 1.25em);
            left: calc(50% - 1.25em);
            height: initial;
            width: initial;
        }

        #lesson-page .code-pre {
            text-align: left;
        }

        .op-40 {
            opacity: .4;
        }

        span.cm-bracket,
        span.cm-tag,
        span.cm-keyword,
        span.cm-error,
        span.cm-link,
        span.cm-string,
        span.cm-attribute,
        span.cm-property,
        span.cm-variable,
        span.cm-variable-2,
        #lesson-page .code-pre {
            color: #ddd;
        }

        #lesson-page img {
            max-width: 300px;
        }

        body,
        #header {
            max-width: 500px;
            margin: auto;
        }

        body {
            background-color: #0f0f0f;
        }

        #ediXtor_page {
            backXground: #1f1f1f;
            paddXing-top: 10px;
            padding-bottom: initial;
        }

        #editor_page,
        #prevXiew_page,
        #profile_page {
            padding-bottom: initial;
            heiXght: calc(100% - 32px);
            height: 100%;
        }

        .CodeMirror {
            font-size: 20px;
            paddXing: 10px 0 0 0;
        }

        .swiper-coXntainer-h {
            padding-top: 0;
            margin-top: 32px;
            box-sizing: border-box;
        }

        .spinner {
            margin: calc(100% - 60px) auto;
            width: 50px;
            height: 40px;
            text-align: center;
            font-size: 10px;
        }

        .spinner > div {
            background-color: #c0c0c0;
            height: 100%;
            width: 6px;
            display: inline-block;

            -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
            animation: sk-stretchdelay 1.2s infinite ease-in-out;
        }

        .spinner .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .spinner .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .spinner .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .spinner .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        @-webkit-keyframes sk-stretchdelay {

            0%,
            40%,
            100% {
                -webkit-transform: scaleY(0.4)
            }

            20% {
                -webkit-transform: scaleY(1.0)
            }
        }

        @keyframes sk-stretchdelay {

            0%,
            40%,
            100% {
                transform: scaleY(0.4);
                -webkit-transform: scaleY(0.4);
            }

            20% {
                transform: scaleY(1.0);
                -webkit-transform: scaleY(1.0);
            }
        }

        .loading {
            background-color: #0b0b0bde;
            height: 100vh;
            width: 100vw;
            top: 0;
            left: 0;
            position: absolute;
            z-index: 1050;
            dispXlay: none;
        }

        #header,
        #footer {

            z-index: 1080;
        }


        .success-layers {
            display: inline-block;
            height: 1em;
            position: relative;
            text-align: center;
            vertical-align: -.125em;
            font-size: 2em;
            text-align: center;
            width: 1.25em;
        }

        .success-svg-icon {
            color: #fff;
            bottom: 0;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0;
        }

        .checkpoint .actions li {
            font-size: 18px;
        }

        .fs-16 {
            font-size: 16px !important;
        }

        #lesson-page .swiper-slide {
            background-color: #000;
            z-index: 1060;
            flex-direction: column;
            justify-content: space-between;
            align-content: center;
            padding: 20px 40px 20px 24px;
        }

        #lesson-page .container {
            padding: initial;
            margin: auto 0;
        }

        #lesson-page .container:first-child:not(:only-child),
        #lesson-page .container:last-child:not(:only-child) {
            margin: 0;
        }

        #lesson-page .container:nth-child(2):last-child {
            margin: auto 0;
        }

        #lesson-page .checkpoint .code-pre {
            color: #333;
            background-color: rgba(255, 255, 255, 0.7);
            margin: 0;
            width: 100%;
        }

        #lesson-page .checkpoint .code-pre span.cm-bracket,
        #lesson-page .checkpoint .code-pre span.cm-tag,
        #lesson-page .checkpoint .code-pre span.cm-keyword,
        #lesson-page .checkpoint .code-pre span.cm-error,
        #lesson-page .checkpoint .code-pre span.cm-link,
        #lesson-page .checkpoint .code-pre span.cm-string,
        #lesson-page .checkpoint .code-pre span.cm-attribute,
        #lesson-page .checkpoint .code-pre span.cm-property,
        #lesson-page .checkpoint .code-pre span.cm-variable,
        #lesson-page .checkpoint .code-pre span.cm-variable-2,
        #lesson-page .checkpoint .code-pre {
            font-size: 18px;
            line-height: 18px;
        }

        #editor {
            /* width: 400px;
            height: 100px; */
            padding: 10px;
            /* padding-left:23px; */
            background-color: #444;
            color: white;
            font-size: 16px;
            font-family: monospace;
        }

        .statement {
            color: orange;
        }

        textarea {
            width: 100%;
            height: 100%;
            background-color: rgb(20, 20, 20);
            color: #fff;
            font-size: 30px;
            border: none;
        }

        .editor_div {
            bottom: 0;
            right: 0;
            left: 0;
            top: 0;
            position: absolute;
        }

        /* #lineNo{
            border: 1px solid red;
            margin-top: 1px;
            position: absolute;
            width: 20px;
            text-align: center;
            color: #ccc;
            height: 96%;
        } */

        /* TLN line-No style */
        .tln-active,
        .tln-wrapper,
        .tln-line {
            margin: 0;
            border: 0;
            padding: 0;
            outline: 0;
            box-sizing: border-box;
            vertical-align: middle;
            list-style: none;
        }

        .tln-active {
            display: inline-block;
            padding: 10px;
            width: calc(100% - 48px);
            height: 100%;
            font-size: 16px;
            line-height: 1.5;
            font-family: "Roboto Mono", monospace;
            word-break: break-all;
            border: 1px solid #aeaeae;
            background-color: #fff;
            resize: none;
            overflow-wrap: normal;
            overflow-x: auto;
            white-space: pre;
        }

        .tln-wrapper {
            width: 48px;
            padding: 11px 5px 35px;
            height: 100%;
            word-break: break-all;
            overflow: hidden;
            display: inline-block;
            counter-reset: line;
            background: #333;
        }

        .tln-line {
            width: 100%;
            display: block;
            text-align: right;
            line-height: 1.5;
            font-size: 16px;
            color: #aeaeae;
        }

        .tln-line::before {
            counter-increment: line;
            content: counter(line);
            font-size: 16px;
            user-select: none;
        }
    </style>


</head>

<body>

<div id="header" class="embed-nav">
    <a href="https://<?php echo $_SERVER['SERVER_NAME'] ?>" style="padding: 0!important;"><img src="/img/logo.png"
                                                                                               alt="logo" id="logo"
                                                                                               style=""/></a>

    <ul id="lesson-navbar" class="hXide2" style="position: absolute; right: 0; top: 0; border: 0;">
        <li id="menu-lesson" class=""><a><span>LESSON</span><i class="icon-assignment"></i></a></li>

        <li id="menu-code"><a><span>CODE</span><i class="icon-code1"></i></a></li>

        <li id="menu-preview"><a><span>PREVIEW</span><i class="icon-personal_video"></i></a></li>

        <li id="menu-skills" class="hide"><a><span>SKILLS</span><i class="icon-school"></i></a></li>
        <li id="menu-profile"><a><span>MENU</span><i class="icon-dehaze"></i></a></li>
    </ul>
</div>
<div class="swiper-container swiper-container-h">
    <div class="swiper-wrapper">
        <div class="swiper-slide" id="lesson-page">
            <div class="swiper-container swiper-container-v">
                <div class="swiper-wrapper cm-s-base16-dark">

                    <div class="loading">
                        <div class="spinner">
                            <div class="rect1"></div>
                            <div class="rect2"></div>
                            <div class="rect3"></div>
                            <div class="rect4"></div>
                            <div class="rect5"></div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination swiper-pagination-v"></div>
            </div>
        </div>
        <div class="swiper-slide" id="editor_page">


            <textarea onchange="editorChange()" id="editor" style="width: 100%; height:100%; display:inherit"
                      autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>


            <div class="row bottom_keys hide">
                <button class="button_design" onclick="btclicked('<')">
                    <
                </button>
                <button class="button_design" onclick="btclicked('>')"> >
                </button>
                <button class="button_design" onclick="btclicked('&#34')"> "</button>
                <button class="button_design" onclick="btclicked('/')"> /</button>
            </div>

        </div>
        <div class="swiper-slide" id="preview_page">
            <iframe class="" id='preview' name="preview"></iframe>

            <div class="swipe-overlay"></div>
        </div>
        <div class="swiper-slide" id="profile_page">
            <div class="container">
                <i class="icon-account_circle fs5" style="padding: 30px 0 10px 0; display: block;"></i>
                <h2 class="profile_name">Hello</h2>
                <div class="menu">
                    <ul>
                        <li class=""><a class="divLink" href="https://<?php echo $_SERVER['SERVER_NAME'] ?>"></a><i
                                    class="icon-home fs2"></i> Homepage
                        </li>
                        <li class="reset-lesson" data-toggle="modal" data-target="#reset-lesson"><i
                                    class="icon-replay fs2"></i> Reset Current Lesson
                        </li>
                        <li class=""><i class="icon-face fs2"></i> Instructor
                            <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                            <div class="option_unselected select_lego">Lego</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="swiper-pagination swiper-pagination-h"></div>
</div>

</div>


<div class="modalPlaceholder"></div>

<div class="success-animation hide" style="">

		<span class="success-layers">
			<svg class="success-svg-icon" style="color: #fff;" aria-hidden="true" data-prefix="fas" data-icon="circle"
                 role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
				<path fill="currentColor"
                      d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
			</svg>
			<svg class="success-svg-icon" style="color: #00A921;" aria-hidden="true" data-prefix="fas"
                 data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                 data-fa-i2svg="">
				<path fill="currentColor"
                      d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path>
			</svg>
		</span>

    </span>
</div>


<script>
    // document.addEventListener('keydown', function(event) {
    // 	console.log(event);
    // 	if (event.keyCode == 37) {
    // 		alert('Left was pressed');
    // 	} else if (event.keyCode == 39) {
    // 		alert('Right was pressed');
    // 	}
    // });
</script>

<script src='/js/merged.js' type='text/javascript' id="js-jquery"></script>

<script type='text/javascript' src='/js/mobile-lesson-P00.js'></script>


<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments)
    }
    gtag('js', new Date());

    gtag('config', 'UA-63106610-3');
    var lesson_id = "P001-L00-M-V002";
    var save_lesson_id = "P000-5MIN";
    var timeout;
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);

        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (swiperTabs.activeIndex == 0) {
                swiperLesson.slideTo(swiperLesson.activeIndex - 1);
                console.log("on lesson tab" + swiperLesson.activeIndex);
            } else {
                swiperTabs.slideTo(swiperTabs.activeIndex - 1);
                console.log("on other tab" + swiperTabs.activeIndex);
            }

        }, 50);
    };

    var is_keyboard = false;
    var initial_screen_size = window.innerHeight;
    window.addEventListener("resize", function () {
        is_keyboard = (window.innerHeight < initial_screen_size);

        console.log(is_keyboard, 'Here');
        if (!is_keyboard) {
            $('textarea').blur();
        }
    }, false);


    function btclicked(symb) {
        insertAtCaret(document.getElementById('editor'), symb);
        console.log("Hello");
    }

    function insertAtCaret(element, text) {
        if (document.selection) {
            console.log('Hello');
            element.focus();
            var sel = document.selection.createRange();
            sel.text = text;
            element.focus();
        } else if (element.selectionStart || element.selectionStart === 0) {
            var startPos = element.selectionStart;
            var endPos = element.selectionEnd;
            var scrollTop = element.scrollTop;
            element.value =
                element.value.substring(0, startPos) +
                text +
                element.value.substring(endPos, element.value.length);
            element.focus();
            element.selectionStart = startPos + text.length;
            element.selectionEnd = startPos + text.length;
            element.scrollTop = scrollTop;
        } else {
            element.value += text;
            element.focus();
        }
    }

    $('textarea').on('keydown', function (e) {
        var keyCode = e.keyCode || e.which;

        if (keyCode === 9) {
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;
            var val = this.value;
            var selected = val.substring(start, end);
            var re = /^/gm;
            var count = selected.match(re).length;


            this.value = val.substring(0, start) + selected.replace(re, '\t') + val.substring(end);
            this.selectionStart = start;
            this.selectionEnd = end + count;
        }
    });

    var myElement = document.getElementById('editor');

    myElement.addEventListener("touchstart", startTouch, false);
    myElement.addEventListener("touchmove", moveTouch, false);

    // Swipe Up / Down / Left / Right
    var initialX = null;
    var initialY = null;

    function startTouch(e) {
        initialX = e.touches[0].clientX;
        initialY = e.touches[0].clientY;
    }

    function moveTouch(e) {
        if (initialX === null) {
            return;
        }

        if (initialY === null) {
            return;
        }

        var currentX = e.touches[0].clientX;
        var currentY = e.touches[0].clientY;

        var diffX = initialX - currentX;
        var diffY = initialY - currentY;

        if (Math.abs(diffX) > Math.abs(diffY)) {
            // sliding horizontally
            if (diffX > 0) {
                // swiped left
                $('textarea').blur();
            } else {
                // swiped right
                // alert('Swipe Right');
                $('textarea').blur();
            }
        } else {
            // sliding vertically
            if (diffY > 0) {
                // swiped up
                console.log("swiped up");
            } else {
                // swiped down
                console.log("swiped down");
            }
        }

        initialX = null;
        initialY = null;

        e.preventDefault();
    }
    // TLN line number code below
    // const TLN = {
    // 	eventList: {},
    // 	update_line_numbers: function(ta, el) {
    // 		let lines = ta.value.split("\n").length;
    // 		let child_count = el.children.length;
    // 		let difference = lines - child_count;

    // 		if (difference > 0) {
    // 			let frag = document.createDocumentFragment();
    // 			while (difference > 0) {
    // 				let line_number = document.createElement("span");
    // 				line_number.className = "tln-line";
    // 				frag.appendChild(line_number);
    // 				difference--;
    // 			}
    // 			el.appendChild(frag);
    // 		}
    // 		while (difference < 0) {
    // 			el.removeChild(el.firstChild);
    // 			difference++;
    // 		}
    // 	},
    // 	append_line_numbers: function(id) {
    // 		let ta = document.getElementById(id);
    // 		if (ta == null) {
    // 			return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
    // 		}
    // 		if (ta.className.indexOf("tln-active") != -1) {
    // 			return console.warn("[tln.js] textarea of id '" + id + "' is already numbered");
    // 		}
    // 		ta.classList.add("tln-active");
    // 		ta.style = {};

    // 		let el = document.createElement("div");
    // 		ta.parentNode.insertBefore(el, ta);
    // 		el.className = "tln-wrapper";
    // 		TLN.update_line_numbers(ta, el);
    // 		TLN.eventList[id] = [];

    // 		const __change_evts = [
    // 			"propertychange", "input", "keydown", "keyup",'focus'
    // 		];
    // 		const __change_hdlr = function(ta, el) {
    // 			return function(e) {
    // 				if ((+ta.scrollLeft == 10 && (e.keyCode == 37 || e.which == 37 ||
    // 						e.code == "ArrowLeft" || e.key == "ArrowLeft")) ||
    // 					e.keyCode == 36 || e.which == 36 || e.code == "Home" || e.key == "Home" ||
    // 					e.keyCode == 13 || e.which == 13 || e.code == "Enter" || e.key == "Enter" ||
    // 					e.code == "NumpadEnter")
    // 					ta.scrollLeft = 0;
    // 				TLN.update_line_numbers(ta, el);
    // 			}
    // 		}(ta, el);
    // 		for (let i = __change_evts.length - 1; i >= 0; i--) {
    // 			ta.addEventListener(__change_evts[i], __change_hdlr);
    // 			TLN.eventList[id].push({
    // 				evt: __change_evts[i],
    // 				hdlr: __change_hdlr
    // 			});
    // 		}

    // 		const __scroll_evts = ["change", "mousewheel", "scroll","focus"];
    // 		const __scroll_hdlr = function(ta, el) {
    // 			return function() {
    // 				el.scrollTop = ta.scrollTop;
    // 			}
    // 		}(ta, el);
    // 		for (let i = __scroll_evts.length - 1; i >= 0; i--) {
    // 			ta.addEventListener(__scroll_evts[i], __scroll_hdlr);
    // 			TLN.eventList[id].push({
    // 				evt: __scroll_evts[i],
    // 				hdlr: __scroll_hdlr
    // 			});
    // 		}
    // 	},
    // 	remove_line_numbers: function(id) {
    // 		let ta = document.getElementById(id);
    // 		if (ta == null) {
    // 			return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
    // 		}
    // 		if (ta.className.indexOf("tln-active") == -1) {
    // 			return console.warn("[tln.js] textarea of id '" + id + "' isn't numbered");
    // 		}
    // 		ta.classList.remove("tln-active");

    // 		ta.previousSibling.remove();

    // 		if (!TLN.eventList[id]) return;
    // 		for (let i = TLN.eventList[id].length - 1; i >= 0; i--) {
    // 			const evt = TLN.eventList[id][i];
    // 			ta.removeEventListener(evt.evt, evt.hdlr);
    // 		}
    // 		delete TLN.eventList[id];
    // 	}
    // }

    // TLN line No. code ends here
</script>
</body>

</html>