<div class="tab-content text-center">
    <div class="tab-pane active" id="slide1" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide01.jpg">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
    </div>
    <div class="tab-pane" id="slide2" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide02.jpg">
    </div>
    <div class="tab-pane" id="slide3" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide03.jpg">
    </div>
    <div class="tab-pane" id="slide4" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide04.jpg">
    </div>
    <div class="tab-pane" id="slide5" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide05.jpg">
    </div>
    <div class="tab-pane" id="slide6" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide06.jpg">
    </div>
    <div class="tab-pane" id="slide7" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide07.jpg">
    </div>
    <div class="tab-pane" id="slide8" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide08.jpg">
    </div>
    <div class="tab-pane" id="slide9" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide09.jpg">
    </div>
    <div class="tab-pane" id="slide10" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide10.jpg">
    </div>
    <div class="tab-pane" id="slide11" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide11.jpg">
    </div>
    <div class="tab-pane" id="slide12" role="tabpanel">
        <div class="checkpoint">
            <h2>Checkpoint #1</h2>
            <ol class="actions">

                <li data-index="0">
                    <span class="check-icon">/</span>
                    Write this in the editor below: &lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;
                </li>

            </ol>
            <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

        </div>

    </div>
    <div class="tab-pane" id="slide13" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide13.jpg">
    </div>
    <div class="tab-pane" id="slide14" role="tabpanel">
        <div class="checkpoint">
            <h2>Checkpoint #2</h2>
            <ol class="actions">

                <li data-index="0">
                    <span class="check-icon">/</span>
                    Write "Hi! I'm Nomzamo Mbatha, an actress. Say hello!" between an opening and a closing paragraph
                    tag
                </li>

            </ol>
            <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

        </div>
    </div>
    <div class="tab-pane" id="slide15" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide15.jpg">
    </div>
    <div class="tab-pane" id="slide16" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide16.jpg">
    </div>
    <div class="tab-pane" id="slide17" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide17.jpg">
    </div>
    <div class="tab-pane" id="slide18" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide18.jpg">
    </div>
    <div class="tab-pane" id="slide19" role="tabpanel">
        <div class="checkpoint">
            <h2>Checkpoint #3</h2>
            <ol class="actions">
                <li data-index="0">
                    <span class="check-icon">/</span>
                    At the bottom, write &lt;input type="email"&gt;
                </li>

            </ol>
            <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

        </div>
    </div>
    <div class="tab-pane" id="slide20" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide20.jpg">
    </div>
    <div class="tab-pane" id="slide21" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide21.jpg">
    </div>
    <div class="tab-pane" id="slide22" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide22.jpg">
    </div>
    <div class="tab-pane" id="slide23" role="tabpanel">
        <div class="checkpoint">
            <h2>Checkpoint #4</h2>
            <ol class="actions">
                <li data-index="0">
                    <span class="check-icon">/</span>
                    Make another input with type="submit"
                </li>

            </ol>
            <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

        </div>
    </div>
    <div class="tab-pane" id="slide24" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide24.jpg">
    </div>
    <div class="tab-pane" id="slide25" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide25.jpg">
    </div>
    <div class="tab-pane" id="slide26" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide26.jpg">
    </div>
    <div class="tab-pane" id="slide27" role="tabpanel">
        <div class="checkpoint">
            <h2>Checkpoint #5</h2>
            <ol class="actions">
                <li data-index="0">
                    <span class="check-icon">/</span>
                    Add placeholder="Your email" to the email input. (If you need a hint, look at the previous slide)
                </li>

            </ol>
            <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

        </div>


    </div>
    <div class="tab-pane" id="slide28" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide28.jpg">
    </div>
    <div class="tab-pane" id="slide29" role="tabpanel">
        <img src="/img/slides-01/HTML-Basics-Slide29.jpg">
    </div>
</div>