 # codejika web-frontend

## Usage

Your best option for running the project is by using  docker


Have a look at the  [docker-compose file](docker-compose.yml) to see how t wire things together


## Structure

    /css

All stylesheets are kept in here


    /js

All javascript files are kept in here

    /codemirror

The codemirror library

TODO ADD MORE EXPLANATIONS

## Development

The codebase is currently written in HTML, Javascript and PHP.


### Running

You have multiple options for running the project for development purposes.

- [Docker](docs/development/running-with-docker.md)
- [Locally](docs/development/running-locally.md)

After successfully starting with docker, the web frontend will be available at 
[http://localhost:8000](http://localhost:8000).



### Configuration

The default configuration should already enable you to start developing.


### Commands

TODO javascript merger and minifier

## Contributing


## LICENSING
Copyright [2020] [Code4Change]
